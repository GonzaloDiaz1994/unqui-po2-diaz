package ar.edu.unq.po2.historiaClinicaSimplificada;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class Atencion {
	private LocalDate fecha = LocalDate.now();
	private String descripcion;
	private Double temperatura;
	private Boolean tos;
	private Boolean dificultadRespiratoria;
	private Opinion opinion;
	private List<Medico> medicos = new ArrayList<Medico>();
	
	public Atencion(String descripcion, Double temperatura, Boolean tos, Boolean dificultadRespiratoria, Opinion opinion) {
		this.descripcion = descripcion;
		this.temperatura = temperatura;
		this.tos = tos;
		this.dificultadRespiratoria = dificultadRespiratoria;
		this.opinion = opinion;
	}
	
	private List<Medico> getMedicos(){
		return this.medicos;
	}
	
	public void agregarMedico(Medico medico) {
		this.getMedicos().add(medico);
		medico.agregarAtenciones(this);
	}
	
	public Opinion getOpinion() {
		return this.opinion;
	}
	
	public LocalDate getFecha() {
		return this.fecha;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}
	
	public Double getTemperatura() {
		return this.temperatura;
	}
	
	public Boolean getTos() {
		return this.tos;
	}
	
	public Boolean getDificultadRespiratoria() {
		return this.dificultadRespiratoria;
	}
	public abstract Boolean getDeRiesgo();
}
