package ar.edu.unq.po2.historiaClinicaSimplificada;

public class AtencionDeGuardia extends Atencion {
	private Boolean deRiesgo = this.esDeRiesgo();

	public AtencionDeGuardia(String descripcion, Double temperatura, Boolean tos, Boolean dificultadRespiratoria, Opinion opinion) {
		super(descripcion, temperatura, tos, dificultadRespiratoria, opinion);
	}

	private Boolean esDeRiesgo() {
		if((this.getTos() || this.getDificultadRespiratoria()) && this.getTemperatura() > 38){
			return true;
		}
		else {
			return false;
		}
	}
	
	public Boolean getDeRiesgo() {
		return this.deRiesgo;
	}
	
}
