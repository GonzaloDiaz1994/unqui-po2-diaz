package ar.edu.unq.po2.historiaClinicaSimplificada;

public class AtencionDeInternacion extends Atencion {
	private Boolean deRiesgo;

	public AtencionDeInternacion(String descripcion, Double temperatura, Boolean tos, Boolean dificultadRespiratoria,
			Opinion opinion, Boolean deRiesgo) {
		super(descripcion, temperatura, tos, dificultadRespiratoria, opinion);
		this.deRiesgo = deRiesgo;
	}
	
	public Boolean getDeRiesgo() {
		return this.deRiesgo;
	}

}
