package ar.edu.unq.po2.historiaClinicaSimplificada;


import java.util.ArrayList;
import java.util.List;


public class HistoriaClinica {
	private String nombre;
	private String apellido;
	private String fechaDeNacimiento;
	private List<Atencion> listaDeAtenciones = new ArrayList<Atencion>();
	
	public HistoriaClinica(String nombre, String apellido, String fechaDeNacimiento) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getApellido() {
		return this.apellido;
	}
	
	public String getFechaDeNacimiento() {
		return this.fechaDeNacimiento;
	}
	
	public List<Atencion> getListaDeAtenciones(){
		return this.listaDeAtenciones;
	}
	
	public void agregarAtencion(Atencion atencion) {
		this.listaDeAtenciones.add(atencion);
	}
	
	public int cantidadDeEvolucionesDeRiesgo() {
		int cantidad = 0;
		for(Atencion atencion: this.getListaDeAtenciones()) {
			if(atencion.getDeRiesgo()) {
				cantidad += 1;
			}else {
				cantidad += 0;
			}
		}
		return cantidad;
	}
	
	public int promedio() {
		int total = 0;
		int cantidadAtenciones = this.getListaDeAtenciones().size();
		for(Atencion atencion: this.getListaDeAtenciones()) {
			total += atencion.getOpinion().getRanking();
		}
		return total / cantidadAtenciones;
	}
	
}
