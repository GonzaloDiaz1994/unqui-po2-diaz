package ar.edu.unq.po2.historiaClinicaSimplificada;

import java.util.ArrayList;
import java.util.List;

public class Medico {
	private Integer matricula;
	private String nombre;
	private String apellido;
	private List<Atencion> atenciones = new ArrayList<Atencion>();
	
	public Medico(Integer matricula, String nombre, String apellido) {
		this.matricula = matricula;
		this.nombre = nombre;
		this.apellido = apellido;
	}
	
	public Integer getMatricula() {
		return this.matricula;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getApellido() {
		return this.apellido;
	}
	
	private List<Atencion> getAtenciones(){
		return this.atenciones;
	}
	
	public void agregarAtenciones(Atencion unaAtencion) {
		this.getAtenciones().add(unaAtencion);
	}
	
	public Double promediosOpiniones() {
		Double suma = 0d;
		for(Atencion atencion: this.getAtenciones()) {
			suma += atencion.getOpinion().getRanking();
		}
		return suma / this.getAtenciones().size();
	}
	
}
