package ar.edu.unq.po2.historiaClinicaSimplificada;

public class Opinion {
	private String descripcion;
	private Integer ranking;
	
	public Opinion(String descripcion, Integer ranking) {
		this.descripcion = descripcion;
		this.ranking = ranking;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}
	
	public Integer getRanking() {
		return this.ranking;
	}
}
