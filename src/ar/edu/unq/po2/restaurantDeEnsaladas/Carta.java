package ar.edu.unq.po2.restaurantDeEnsaladas;

import java.util.ArrayList;
import java.util.List;

public class Carta {
	private String nombre;
	private List<Plato> platosOfrecidos = new ArrayList<Plato>();
	
	
	public Carta(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	private List<Plato> getPlatosOfrecidos(){
		return this.platosOfrecidos;
	}
	
	public void agregarPlatos(Plato plato) {
		this.getPlatosOfrecidos().add(plato);
	}

	public Double sumaPlatosActivos() {
		Double suma = 0d;
		for(Plato plato: this.getPlatosOfrecidos()) {
			if(plato.getActivoParaLaCompra()) {
				suma += plato.precioFinal();
			}else {
				suma += 0;
			}
		}
		return suma;
	}
}
