package ar.edu.unq.po2.restaurantDeEnsaladas;

public class Chef {
	private String nombre;
	private Integer ranking;
	
	public Chef(String nombre, Integer ranking) {
		this.nombre = nombre;
		this.ranking = ranking;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Integer getRanking() {
		return this.ranking;
	}
}
