package ar.edu.unq.po2.restaurantDeEnsaladas;

public class Opinion {
	private String texto;
	private Integer ranking;
	
	public Opinion(String texto, Integer ranking) {
		this.texto = texto;
		this.ranking = ranking;
	}
	
	public String getTexto() {
		return this.texto;
	}
	
	public Integer getRanking() {
		return this.ranking;
	}
}
