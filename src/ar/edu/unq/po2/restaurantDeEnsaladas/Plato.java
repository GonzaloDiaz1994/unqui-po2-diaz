package ar.edu.unq.po2.restaurantDeEnsaladas;

import java.util.ArrayList;
import java.util.List;

public abstract class Plato {
	private String nombre;
	private Integer cantidadDeIngredientes;
	private Double precioBase;
	private Boolean activoParaLaCompra = true;
	private List<Opinion> opiniones = new ArrayList<Opinion>();
	
	public Plato(String nombre, Integer cantidadDeIngredientes, Double precioBase) {
		this.nombre= nombre;
		this.cantidadDeIngredientes = cantidadDeIngredientes;
		this.precioBase = precioBase;
		
	}
	
	public Plato(String nombre, Double precioBase) {
		this.nombre = nombre;
		this.precioBase = precioBase;
	}
	
	public void agregarOpinion(Opinion unaOpinion) {
		this.opiniones.add(unaOpinion);
	}
	
	private List<Opinion> getOpiniones() {
		return this.opiniones;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Integer getCantidadDeIngredientes() {
		return this.cantidadDeIngredientes;
	}
	
	public Double getPrecioBase() {
		return this.precioBase;
	}
	
	public Boolean getActivoParaLaCompra() {
		return this.activoParaLaCompra;
	}
	
	public void setActivoParaLaCompra(Boolean bool) {
		this.activoParaLaCompra = bool;
	}
	
	public abstract Double precioFinal();
	
	public Integer promedio() {
		Integer suma = 0;
		for(Opinion opinion: this.getOpiniones()) {
			suma += opinion.getRanking();
		}
		return suma / this.getOpiniones().size();
	}
	
}
