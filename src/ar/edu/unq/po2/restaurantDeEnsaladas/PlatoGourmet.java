package ar.edu.unq.po2.restaurantDeEnsaladas;

public class PlatoGourmet extends Plato {
	private Chef chef;

	public PlatoGourmet(String nombre, Double precioBase, Chef chef) {
		super(nombre, precioBase);
		this.chef = chef;
	}

	public Chef getChef() {
		return this.chef;
	}
	
	@Override
	public Double precioFinal() {
		return (this.getPrecioBase() + (100 * chef.getRanking()));
	}

}
