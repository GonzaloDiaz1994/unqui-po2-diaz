package ar.edu.unq.po2.restaurantDeEnsaladas;


public class PlatoTradicional extends Plato {

	public PlatoTradicional(String nombre, Integer cantidadDeIngredientes, Double precioBase) {
		super(nombre, cantidadDeIngredientes, precioBase);
	}

	@Override
	public Double precioFinal() {
		if(this.getCantidadDeIngredientes() <= 3) {
			return this.getPrecioBase() * 1.2;
		}else {
			return this.getPrecioBase() * 1.4;
		}
	}

}
