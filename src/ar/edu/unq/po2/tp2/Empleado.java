package ar.edu.unq.po2.tp2;

import java.time.LocalDate;
import java.time.Period;

public abstract class Empleado {
	
	private String nombre;
	private String direccion;
	private String estadoCivil;
	private String fechaDeNacimiento;
	private double sueldoBasico;
	
	public Empleado(String nombre, String direccion, String estadoCivil, String fechaDeNacimiento,
			double sueldoBasico) {
		this.setNombre(nombre);
		this.setDireccion(direccion);
		this.setEstadoCivil(estadoCivil);
		this.setFechaDeNacimiento(fechaDeNacimiento);
		this.setSueldoBasico(sueldoBasico);
		
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDireccion() {
		return this.direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getEstadoCivil() {
		return this.estadoCivil;
	}
	
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	
	public String getFechaDeNacimiento() {
		return this.fechaDeNacimiento;
	}
	
	public void setFechaDeNacimiento(String fechaNacimiento) {
		this.fechaDeNacimiento = fechaNacimiento;
	}
	
	public double getSueldoBasico() {
		return this.sueldoBasico;
	}
	
	public void setSueldoBasico(double sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}
	
	public int edad() {
		LocalDate ahora = LocalDate.now();
		LocalDate fechaNac =  LocalDate.parse(this.getFechaDeNacimiento());		
		Period tiempo = Period.between(fechaNac, ahora);
		return tiempo.getYears();
	}
	
	public abstract double sueldoBruto();
	
	public abstract double retenciones();
	
	public abstract double obraSocial();
	
	public abstract double aportesJubilatorios();
	
	public double sueldoNeto() {
		return this.sueldoBruto() - this.retenciones();
	}
}