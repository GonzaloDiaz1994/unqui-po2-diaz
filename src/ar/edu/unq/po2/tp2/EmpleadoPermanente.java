package ar.edu.unq.po2.tp2;

public class EmpleadoPermanente extends Empleado {

	private int cantidadDeHijos;
	private int aniosAntiguedad;
	private boolean tieneConyugue;
	
	public EmpleadoPermanente(String nombre, String direccion, String estadoCivil, String fechaDeNacimiento,
			double sueldoBasico, int cantidadDeHijos, int aniosAntiguedad, boolean tieneConyugue) {
		super(nombre, direccion, estadoCivil, fechaDeNacimiento, sueldoBasico);
		this.setCantidadDeHijos(cantidadDeHijos);
		this.setAniosAntiguedad(aniosAntiguedad);
		this.setTieneConyugue(tieneConyugue);
	}
	
	private void setTieneConyugue(boolean tieneConyugue2) {
		this.tieneConyugue = tieneConyugue2;
	}

	private void setAniosAntiguedad(int aniosAntiguedad2) {
		this.aniosAntiguedad = aniosAntiguedad2;
	}

	private void setCantidadDeHijos(int cantidadDeHijos2) {
		this.cantidadDeHijos = cantidadDeHijos2;
	}

	public int getCantidadDeHijos() {
		return this.cantidadDeHijos;
	}
	
	public int getAniosAntiguedad() {
		return this.aniosAntiguedad;
	}
	
	public double asignacionPorHijo() {
		return this.getCantidadDeHijos() * 150;
	}
	
	public double asignacionPorConyugue() {
		if(this.getTieneConyugue()) {
			return 100;
		}
		else {
			return 0;
		}
	}
	
	public double extraPorAntiguedad() {
		return this.getAniosAntiguedad() * 50;
	}
	
	public double salarioFamiliar() {
		return this.asignacionPorHijo() + this.asignacionPorConyugue();
	}
	
	public Boolean getTieneConyugue() {
		return this.tieneConyugue;
	}
	
	@Override
	public double sueldoBruto() {
		return this.getSueldoBasico() + this.salarioFamiliar() + this.extraPorAntiguedad();
	}
	
	@Override
	public double obraSocial() {
		return  ((this.sueldoBruto()* 0.1) + (this.getCantidadDeHijos() * 20));
	}
	
	@Override
	public double aportesJubilatorios() {
		return this.sueldoBruto() * 0.15;
	}
	
	@Override
	public double retenciones() {
		return this.obraSocial() + this.aportesJubilatorios();
	}

}
