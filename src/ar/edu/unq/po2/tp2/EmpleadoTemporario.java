package ar.edu.unq.po2.tp2;


public class EmpleadoTemporario extends Empleado {
	
	private String fechaFinDeContrato;
	private int cantidadHorasExtras;

	public EmpleadoTemporario(String nombre, String direccion, String estadoCivil, String fechaDeNacimiento,
			double sueldoBasico, String fechaFinDeContrato, int cantidadHorasExtras) {
		super(nombre, direccion, estadoCivil, fechaDeNacimiento, sueldoBasico);
		this.setFechaFinDeContrato(fechaFinDeContrato);
		this.setCantidadHorasExtras(cantidadHorasExtras);
	}

	private void setCantidadHorasExtras(int cantidadHorasExtras2) {
		this.cantidadHorasExtras = cantidadHorasExtras2;
	}

	public String getFechaFinDeContrtato() {
		return this.fechaFinDeContrato;
	}
	
	public void setFechaFinDeContrato(String fechaFin) {
		this.fechaFinDeContrato = fechaFin;
	}
	
	public int getCantidadHorasExtras() {
		return this.cantidadHorasExtras;
	}
	
	private int horasExtras() {
		return this.getCantidadHorasExtras() * 40;
	}
	
	@Override
	public double sueldoBruto() {
		return this.getSueldoBasico() + this.horasExtras();
	}

	@Override
	public double obraSocial() {
		if(edad() > 50) {
			return (this.sueldoBruto() * 0.1) + 25;
		}
		else {
			return this.sueldoBruto() * 0.1;
		}
	}

	@Override
	public double aportesJubilatorios() {
		return (this.sueldoBruto() * 0.1) + (this.getCantidadHorasExtras() * 5);
	}

	@Override
	public double retenciones() {
		return this.obraSocial() + this.aportesJubilatorios();
	}
}
