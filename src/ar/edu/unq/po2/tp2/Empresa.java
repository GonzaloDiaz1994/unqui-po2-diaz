package ar.edu.unq.po2.tp2;

import java.util.ArrayList;
import java.util.List;

public class Empresa {
	
	private String nombre;
	private int cuit;
	private List<Empleado> empleados = new ArrayList<Empleado>(); 
	private List<ReciboDeHaberes> recibosDeHaberes = new ArrayList<ReciboDeHaberes>();
	
	public Empresa(String nombre, int cuit) {
		this.nombre = nombre;
		this.cuit = cuit;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public int getCuit() {
		return this.cuit;
	}
	
	public List<Empleado> getEmpleados() {
		return this.empleados;
	}
	
	public List<ReciboDeHaberes> getReciboDeHaberes() {
		return this.recibosDeHaberes;
	}
	
	public void agregarRecibo(ReciboDeHaberes recibo) {
		this.recibosDeHaberes.add(recibo);
	}
	
	public void agregarEmpleado(Empleado empleado) {
		this.empleados.add(empleado);
	}
	
	public void eliminarEmpleado(Empleado empleado) {
		this.getEmpleados().remove(empleado);
	}
	
	public double montoTotalSueldosNetos() {
		double total = 0;
		for(Empleado empleado: this.getEmpleados()) {
			total += empleado.sueldoNeto();
		}
		return total;
	}
	
	public double montoTotalSueldosBrutos() {
		double total = 0;
		for(Empleado empleado: this.getEmpleados()) {
			total += empleado.sueldoBruto();
		}
		return total;
	}
	
	public double montoTotalRetenciones() {
		double total = 0;
		for(Empleado empleado: this.getEmpleados()) {
			total += empleado.retenciones();
		}
		return total;
	}
	
	// rompe SOLID?
	public void lisquidacionDeSueldos() {
		for(Empleado empleado : this.getEmpleados()) {
			ReciboDeHaberes recibo = new ReciboDeHaberes( empleado.getNombre(), empleado.getDireccion(),
					empleado.sueldoBruto(), empleado.sueldoNeto(),
					"Sueldo");
			this.agregarRecibo(recibo);
		}
	}
}
