package ar.edu.unq.po2.tp2;

import java.time.LocalDate;

public class ReciboDeHaberes {
	private String  nombreEmpleado;
	private String direccionEmpleado;
	private LocalDate fechaEmisionDelRecibo;
	private double sueldoBruto;
	private double sueldoNeto;
	private String desgloceDeConceptos;
	
	public ReciboDeHaberes(String nombreEmpleado, String direccionEmpleado, double sueldoBruto, double sueldoNeto,
			String desgloceDeConceptos) {
		this.setNombreEmpleado(nombreEmpleado);
		this.setdireccionEmpleado(direccionEmpleado);
		this.setSueldoBruto(sueldoBruto);
		this.setSueldoNeto(sueldoNeto);
		this.setDesgloceDeConceptos(desgloceDeConceptos);
		this.setFechaEmisionDelRecibo(LocalDate.now());
	}
	
	private void setFechaEmisionDelRecibo(LocalDate now) {
		this.fechaEmisionDelRecibo = now;
	}

	private void setDesgloceDeConceptos(String desgloceDeConceptos2) {
		this.desgloceDeConceptos = desgloceDeConceptos2;	
	}

	private void setSueldoNeto(double sueldoNeto2) {
		this.sueldoNeto = sueldoNeto2;
	}

	private void setSueldoBruto(double sueldoBruto2) {
		this.sueldoBruto = sueldoBruto2;		
	}

	private void setdireccionEmpleado(String direccionEmpleado2) {
		this.direccionEmpleado = direccionEmpleado2;		
	}

	private void setNombreEmpleado(String nombreEmpleado2) {
		this.nombreEmpleado = nombreEmpleado2;
	}

	public String getNombreEmpleado() {
		return this.nombreEmpleado;
	}
	
	public String getDireccionEmpleado() {
		return this.direccionEmpleado;
	}
	
	public LocalDate getFechaDeEmisionDelRecibo() {
		return this.fechaEmisionDelRecibo;
	}
	
	public double getSueldoBruto() {
		return this.sueldoBruto;
	}
	
	public double getSueldoNeto() {
		return this.sueldoNeto;
	}
	
	public String getDesgloceDeConceptos() {
		return this.desgloceDeConceptos;
	}
}
