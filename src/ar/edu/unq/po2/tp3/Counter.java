package ar.edu.unq.po2.tp3;

import java.util.ArrayList;
import java.util.List;

public class Counter {
	private List<Integer> numeros = new ArrayList<Integer>();
	
	public List<Integer> getNumeros(){
		return this.numeros;
	}
	
	public void agregarNumero(Integer numero) {
		this.getNumeros().add(numero);
	}
	
	public Integer cantidadPares() {
		Integer cant = 0;
		for(Integer numero: this.getNumeros()) {
			if(numero % 2 == 0) {
				cant += 1;
			}else {
				cant+=0;
			}
		}
		return cant;
	}
	
	public Integer cantidadImpares() {
		Integer cant = 0;
		for(Integer numero: this.getNumeros()) {
			if(numero % 2 != 0) {
				cant += 1;
			}else {
				cant += 0;
			}
		}
		return cant;
	}
	
	public Integer cantidadMultiplosDe(Integer unNumero) {
		Integer cant = 0;
		for(Integer numero: this.getNumeros()) {
			if(numero % unNumero == 0) {
				cant += 1;
			}else {
				cant += 0;
			}
		}
		return cant;
	}
	
}
