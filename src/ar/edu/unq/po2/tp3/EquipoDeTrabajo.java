package ar.edu.unq.po2.tp3;

import java.util.ArrayList;
import java.util.List;

public class EquipoDeTrabajo {
	private String nombre;
	private List<Persona> integrantes = new ArrayList<Persona>();
	
	public EquipoDeTrabajo(String nombre) {
		this.nombre = nombre;
	}
	
	public String getnombre() {
		return this.nombre;
	}
	
	public List<Persona> getIntegrantes() {
		return this.integrantes;
	}
	
	public void agregarIntegrante(Persona unaPersona) {
		this.getIntegrantes().add(unaPersona);
	}
	
	public Double promedioDeEdad() {
		Double total = 0d;
		for(Persona persona: this.getIntegrantes()) {
			total += persona.edad();
		}
		return total / this.getIntegrantes().size();
	}
	
}
