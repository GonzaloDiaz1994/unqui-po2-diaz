package ar.edu.unq.po2.tp3;

import java.util.ArrayList;
import java.util.List;

public class Multioperador {
	private List<Integer> numeros = new ArrayList<Integer>();
	
	public List<Integer> getNumeros() {
		return this.numeros;
	}
	
	public void agregarNumero(Integer unNumero) {
		this.getNumeros().add(unNumero);
	}
	
	public Integer sumarNumeros() {
		Integer total = 0;
		for(Integer numero: this.getNumeros()) {
			total += numero;
		}
		return total;
	}
	
	public Integer restarNumeros() {
		Integer total = this.getNumeros().get(0);
		for(Integer numero: this.getNumeros()) {
			total = total - numero;
		}
		return total + this.getNumeros().get(0);
	}
	
	public Integer multiplicarNumeros() {
		Integer total = this.getNumeros().get(0);
		for(Integer numero: this.getNumeros()) {
			total = total * numero;
		}
		return total / this.getNumeros().get(0);
	}
}
