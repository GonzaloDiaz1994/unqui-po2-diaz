package ar.edu.unq.po2.tp3;

import java.time.LocalDate;
import java.time.Period;

public class Persona {
	private String nombre;
	private String fechaDeNacimiento;
	
	public Persona(String nombre, String fechaDeNacimiento) {
		this.nombre = nombre;
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getFechaDeNacimiento() {
		return this.fechaDeNacimiento;
	}
	
	public Integer edad() {
		LocalDate ahora = LocalDate.now();
		LocalDate fechaNac =  LocalDate.parse(this.getFechaDeNacimiento());		
		Period tiempo = Period.between(fechaNac, ahora);
		return tiempo.getYears();
	}
	
	public Boolean menorQue(Persona unaPersona) {
		return this.edad() < unaPersona.edad();
	}
}
