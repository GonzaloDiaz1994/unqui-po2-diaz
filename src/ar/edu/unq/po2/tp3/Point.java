package ar.edu.unq.po2.tp3;

public class Point {
	private Integer x;
	private Integer y;
	
	public Point(Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}
	
	public Point() {
		this.x = 0;
		this.y = 0;
	}
	
	public Integer getX() {
		return this.x;
	}
	
	public Integer getY() {
		return this.y;
	}
	
	public void setX(Integer unaX) {
		this.x = unaX;
	}
	
	public void setY(Integer unaY) {
		this.y = unaY;
	}
		
	public void moverPunto(Integer unaX, Integer unaY) {
		this.setX(unaX);
		this.setY(unaY);
	}
	
	public Point sumarConOtroPunto(Point unPunto) {
		Integer otraX = unPunto.getX();
		Integer otraY = unPunto.getY();
		Point nuevoPunto = new Point();
		nuevoPunto.setX(this.getX() + otraX);
		nuevoPunto.setY(this.getY() + otraY);
		return nuevoPunto;		
	}
}
