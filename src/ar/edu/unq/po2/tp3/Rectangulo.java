package ar.edu.unq.po2.tp3;

public class Rectangulo {
	private Point origen;
	private Point esquinaOpuesta;
	
	public Rectangulo(Point origen, Point esquinaOpuesta) {
		this.origen = origen;
		this.esquinaOpuesta = esquinaOpuesta;		
	}
	
	public Integer calcularArea() {
		return this.base() * this.altura();
	}
	
	public Integer calcularPerimetro() {
		return (this.base() + this.altura()) * 2;
	}
	
	public Integer base() {
		return Math.abs(this.origen.getX() - this.esquinaOpuesta.getX());
	}
	
	public Integer altura() {
		return Math.abs(this.origen.getY() - this.esquinaOpuesta.getY());
	}
	
	public Boolean esHorizontal() {
		return this.base() > this.altura();
	}
	
	public Boolean esVertical() {
		return this.base() < this.altura();
	}
	
}
