package ar.edu.unq.po2.tp4;

public class Producto {
	protected String nombre;
	protected double precio;
	protected boolean perteneceAPreciosCuidados = false;

	public Producto(String nombre, double precio, boolean perteneceAPreciosCuidados) {
		this.nombre = nombre;
		this.precio = precio;
		this.perteneceAPreciosCuidados = perteneceAPreciosCuidados;
	}

	public Producto(String nombre, double precio) {
		this.nombre = nombre;
		this.precio = precio;
	}

	public String getNombre() {
		return this.nombre;
	}

	public Double getPrecio() {
		return this.precio;
	}

	public boolean esPrecioCuidado() {
		return this.perteneceAPreciosCuidados;
	}

	public void aumentarPrecio(double d) {
		this.precio += d;
		
	}

}
