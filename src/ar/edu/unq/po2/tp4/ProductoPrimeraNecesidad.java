package ar.edu.unq.po2.tp4;

public class ProductoPrimeraNecesidad extends Producto{
	
	private double descuento;
	
	public ProductoPrimeraNecesidad(String nombre, double precio, boolean perteneceAPreciosCuidados, double descuento) {
		super(nombre, precio, perteneceAPreciosCuidados);
		this.descuento = descuento;
	}
	
	public ProductoPrimeraNecesidad(String nombre, double precio, double descuento) {
		super(nombre, precio);
		this.descuento = descuento;
	}
	
	@Override
	public Double getPrecio() {
		return this.precio - (this.descuento/100 * 10);
	}

}
