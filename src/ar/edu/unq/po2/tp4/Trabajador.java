package ar.edu.unq.po2.tp4;

import java.util.ArrayList;
import java.util.List;

public class Trabajador {
	private List<Ingreso> ingresosPercibidos = new ArrayList<Ingreso>();
	
	public Trabajador() {
		
	}
	
	public void agregarIngreso(Ingreso unIngreso) {
		this.ingresosPercibidos.add(unIngreso);
	}
	
	public Double getTotalPercibido() {
		Double total = 0d;
		for(Ingreso ingreso: this.ingresosPercibidos) {
			total += ingreso.getMontoPercibido();
		}
		return total;
	}
	
	public Double getMontoImponible() {
		Double total =0d;
		for(Ingreso ingreso : this.ingresosPercibidos) {
				total += ingreso.getMontoImponible();
			}
		return total;
	}
	
	public Double getImpuestoAPagar() {
		return this.getMontoImponible() * 0.02;
	}
	
}
