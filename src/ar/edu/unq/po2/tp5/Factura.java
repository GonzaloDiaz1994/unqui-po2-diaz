package ar.edu.unq.po2.tp5;

public abstract class Factura {

	public Factura() {
	}
	
	public abstract Integer getMontoAPagar();

}
