package ar.edu.unq.po2.tp5;

public class Impuesto extends Factura{

	private Integer tasaServicio;
	
	public Impuesto(Integer tasaServicio) {
		this.tasaServicio = tasaServicio;
	}
	
	public Integer getMontoAPagar() {
		return this.tasaServicio;
	}

}
