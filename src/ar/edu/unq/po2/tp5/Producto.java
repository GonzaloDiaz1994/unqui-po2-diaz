package ar.edu.unq.po2.tp5;

public abstract class Producto{
	private Double precio;
	private Integer stock;
	
	public Producto(Double precio, Integer stock) {
		this.precio = precio;
		this.setStock(stock);
	}

	public Double getPrecio() {
		return precio;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public abstract Double getMontoAPagar();
	
}
