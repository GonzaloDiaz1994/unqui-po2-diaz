package ar.edu.unq.po2.tp5;

public class ProductoDeCooperativa extends Producto{

	public ProductoDeCooperativa(Double precio, Integer stock) {
		super(precio, stock);
	}
	
	@Override
	public Double getMontoAPagar() {
		Double descuento = (this.getPrecio() * 0.10d);
		return this.getPrecio() - descuento;
	}

}
