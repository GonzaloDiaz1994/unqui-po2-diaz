package ar.edu.unq.po2.tp5;

public class ProductoTradicional extends Producto {

	public ProductoTradicional(Double precio, Integer stock) {
		super(precio, stock);
	}

	@Override
	public Double getMontoAPagar() {
		return this.getPrecio();
	}

}
