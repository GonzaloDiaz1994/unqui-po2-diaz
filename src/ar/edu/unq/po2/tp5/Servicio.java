package ar.edu.unq.po2.tp5;

public class Servicio extends Factura {
	
	private Integer costoUnidad;
	private Integer unidadConsumida;
	
	public Servicio(Integer costoUnidad, Integer unidadConsumida) {
		this.costoUnidad = costoUnidad;
		this.unidadConsumida = unidadConsumida;
	}
	
	public Integer getMontoAPagar() {
		return this.costoUnidad * this.unidadConsumida;
	}

}
