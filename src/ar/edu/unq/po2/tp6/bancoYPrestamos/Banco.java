package ar.edu.unq.po2.tp6.bancoYPrestamos;

import java.util.ArrayList;
import java.util.List;

public class Banco {
	private List<Cliente> clientes;
	private List<SolicitudDeCredito> solicitudes;

	public Banco() {
		this.clientes = new ArrayList<Cliente>();
		this.solicitudes = new ArrayList<SolicitudDeCredito>();
	}
	
	public void agregarCliente(Cliente unCliente) {
		this.clientes.add(unCliente);
	}
	
	public void agregarSolicitudDeCredito(SolicitudDeCredito unaSolicitud) {
		this.solicitudes.add(unaSolicitud);
	}
	
	public Integer montoTotalADesembolsar() {
		Integer total = 0;
		for(SolicitudDeCredito solicitud: this.getSolicitudes()) {
			if((esCliente(solicitud.cliente)) && (solicitud.esAceptable())) {
				total += solicitud.getMontoSolicitado();
			}else {
				total += 0;
			}
		}return total;
	}
	
	public void evaluarSolicitudes() {
		for(SolicitudDeCredito solicitud: this.getSolicitudes()) {
			if((esCliente(solicitud.cliente)) && (solicitud.esAceptable())) {
				this.otorgarCredito(solicitud);
			}else {
				this.solicitudes.remove(solicitud);
			}
		}this.getSolicitudes().clear();
	}
	
	public void otorgarCredito(SolicitudDeCredito solicitud) {
		solicitud.cliente.setEfectivo(solicitud.montoSolicitado);
	}
	
	public Boolean esCliente(Cliente cliente) {
		return this.clientes.contains(cliente);
	}

	public List<SolicitudDeCredito> getSolicitudes() {
		return this.solicitudes;
	}
}

