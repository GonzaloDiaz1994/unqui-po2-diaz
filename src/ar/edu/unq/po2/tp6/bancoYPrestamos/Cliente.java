package ar.edu.unq.po2.tp6.bancoYPrestamos;

public class Cliente {

	private String nombre;
	private String apellido;
	private String direccion;
	private Integer edad;
	private Integer efectivo;
	private Integer sueldoNetoMensual;
	private Propiedad propiedad;
	
	public Cliente(String nombre, String apellido, String direccion, Integer edad,
			Integer sueldoNetoMensual, Propiedad propiedad) {
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setDireccion(direccion);
		this.setEdad(edad);
		this.setEfectivo(0);
		this.setSueldoNetoMensual(sueldoNetoMensual);
		this.setPropiedad(propiedad);
	}
	
	private void setPropiedad(Propiedad propiedad2) {
		this.propiedad = propiedad2;
	}

	private void setSueldoNetoMensual(Integer sueldoNetoMensual2) {
		this.sueldoNetoMensual = sueldoNetoMensual2;
	}

	private void setEdad(Integer edad2) {
		this.edad = edad2;
	}

	private void setApellido(String apellido2) {
		this.apellido = apellido2;
		
	}

	private void setNombre(String nombre2) {
		this.nombre = nombre2;
		
	}

	public Integer sueldoAnual() {
		return this.sueldoNetoMensual * 12;
	}
	
	public SolicitudDeCredito solicitarCreditoPersonal(Integer monto, Integer plazoEnMeses) {
		return new SolicitudCreditoPersonal(this, monto, plazoEnMeses);
	}
	
	public SolicitudDeCredito solicitarCreditoHipotecario(Integer monto, Integer plazo ) {
		return new SolicitudCreditoHipotecario(this, this.edad, monto, this.propiedad);
	}

	public Integer getSueldoMensual() {
		return this.sueldoNetoMensual;
	}

	public int getEdad() {
		return this.edad;
	}

	public void setEfectivo(Integer montoSolicitado) {
		this.efectivo = montoSolicitado;
		
	}

	public Integer getEfectivo() {
		return this.efectivo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
