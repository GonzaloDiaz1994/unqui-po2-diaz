package ar.edu.unq.po2.tp6.bancoYPrestamos;

public class Propiedad {
	private String descripcion;
	private String direccion;
	private Integer valorFiscal;
	
	public Propiedad(String descripcion, String direccion, Integer valorFiscal) {
		this.setDescripcion(descripcion);
		this.direccion = direccion;
		this.valorFiscal = valorFiscal;
	}

	public Double getValorFiscal() {
		return(double) this.valorFiscal;
	}

	public String getDireccion() {
		return direccion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
