package ar.edu.unq.po2.tp6.bancoYPrestamos;

public class SolicitudCreditoHipotecario extends SolicitudDeCredito {
	private Propiedad propiedad;

	public SolicitudCreditoHipotecario(Cliente cliente, Integer monto, Integer plazo,
			Propiedad unaPropiedad) {
		super(cliente, monto, plazo);
		this.propiedad = unaPropiedad;
	}

	@Override
	public Boolean esAceptable() {
		return
			this.cuotaNoSuperaPorcentaje(50) &&
			this.montoSolicitadoNoSuperaPorcentaje(70) &&
			(this.cliente.getEdad() + this.plazoEnAnios() <= 65);		
	}
	
	public Boolean montoSolicitadoNoSuperaPorcentaje(Integer unPorcentaje) {
		return
			(this.propiedad.getValorFiscal()* (unPorcentaje / 100d))
			> this.montoSolicitado;
	}

}
