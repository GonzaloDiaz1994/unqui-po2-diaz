package ar.edu.unq.po2.tp6.bancoYPrestamos;

public class SolicitudCreditoPersonal extends SolicitudDeCredito {

	public SolicitudCreditoPersonal(Cliente cliente, Integer monto, Integer plazo) {
		super(cliente, monto, plazo);
	}

	@Override
	public Boolean esAceptable() {
		return
				(this.cliente.sueldoAnual() > 15000) &&
				(this.cuotaNoSuperaPorcentaje(70));
	}

}
