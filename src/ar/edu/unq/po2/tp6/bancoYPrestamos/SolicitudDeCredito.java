package ar.edu.unq.po2.tp6.bancoYPrestamos;

public abstract class SolicitudDeCredito {
	protected Cliente cliente;
	protected Integer montoSolicitado;
	protected Integer plazoEnMeses;
	
	public SolicitudDeCredito(Cliente cliente, Integer monto,
			Integer plazo) {
		this.cliente = cliente;
		this.montoSolicitado = monto;
		this.plazoEnMeses = plazo;
	}
	
	public Double montoCuotaMensual() {
		return (double) (this.montoSolicitado / this.plazoEnMeses);
	}
	
	public abstract Boolean esAceptable();
	
	public Boolean cuotaNoSuperaPorcentaje(Integer unPorcentaje) {
		Double porcentaje = (double) (unPorcentaje / 100d);
		return
				(this.cliente.getSueldoMensual() * porcentaje)
				> this.montoCuotaMensual();
	}
	
	public Double plazoEnAnios() {
		return (double) (this.plazoEnMeses / 12);
	}

	public Integer getMontoSolicitado() {
		return this.montoSolicitado;
	}
}
