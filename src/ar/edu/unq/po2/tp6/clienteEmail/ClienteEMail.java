package ar.edu.unq.po2.tp6.clienteEmail;

import java.util.ArrayList;

public class ClienteEMail {
	
	private IServidor servidor; 
	private String nombreUsuario;
	private String passusuario;
	private Boolean estaConectado;
	private ArrayList<Correo> inbox;
	private ArrayList<Correo> borrados;
	
	public ClienteEMail(IServidor servidor, String nombreUsuario, String pass){
		this.setServidor(servidor);
		this.setNombreUsuario(nombreUsuario);
		this.setPassUsuario(pass);
		this.setEstaConectado(false);
		this.inbox = new ArrayList<Correo>();
		this.borrados = new ArrayList<Correo>();
	}
	
	private void setEstaConectado(boolean b) {
		this.estaConectado = b;		
	}
	public Boolean getEstaConectado() {
		return this.estaConectado;
	}

	private void setPassUsuario(String pass) {
		this.passusuario = pass;
	}

	private void setNombreUsuario(String nombreUsuario2) {
		this.nombreUsuario = nombreUsuario2;
	}

	private void setServidor(IServidor servidor2) {
		this.servidor = servidor2;		
	}
	
	public IServidor getServidor() {
		return this.servidor;
	}

	public ArrayList<Correo> getInbox(){
		return this.inbox;
	}
	
	public ArrayList<Correo> getBorrados(){
		return this.borrados;
	}

	public String getNombreUsuario() {
		return this.nombreUsuario;
	}
	
	public String getPassUsuario() {
		return this.passusuario;
	}
	
	public void conectar(){
		this.getServidor().conectar(this.getNombreUsuario(),this.getPassUsuario()); 
	}
	
	public void iniciarSesion() {
		this.setEstaConectado(true);
	}
	
	public void borrarCorreo(Correo correo){
		this.getInbox().remove(correo);
		this.getBorrados().add(correo);
	}
	
	public int contarBorrados(){
		return this.getBorrados().size();
	}
	
	public int contarInbox(){
		return this.getInbox().size();
	}
	
	public void eliminarBorrado(Correo correo){
		this.getBorrados().remove(correo);
	}
	
	public void recibirNuevos(){
		this.getServidor().recibirNuevos(this);
	}
	
	public void enviarCorreo(Correo unCorreo){
		this.getServidor().enviar(unCorreo);
	}
	
	public void agregarCorreo(Correo correo) {
		this.getInbox().add(correo);
	}

}
