package ar.edu.unq.po2.tp6.clienteEmail;

import java.util.List;

public interface IServidor {
	
	public List<Correo> recibirNuevos(ClienteEMail unCliente);

	public void conectar(String nombreUsuario, String passusuario);

	public void enviar(Correo correo);
	
	public float tazaDeTransferencia();

	public void resetear();
	
	public void realizarBackUp();

}
