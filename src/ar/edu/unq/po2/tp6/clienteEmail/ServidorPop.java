package ar.edu.unq.po2.tp6.clienteEmail;

import java.util.ArrayList;
import java.util.List;


public class ServidorPop implements IServidor {
	
	private List<ClienteEMail> baseDeDatos;
	
	public ServidorPop() {
		this.baseDeDatos = new ArrayList<ClienteEMail>();
	}
	
	public List<ClienteEMail> getBaseDeDatos(){
		return this.baseDeDatos;
	}
	
	public void agregarUsuario(ClienteEMail cliente) {
		this.baseDeDatos.add(cliente);
	}

	public List<Correo> recibirNuevos(ClienteEMail unCliente) {
		List<Correo> retorno = new ArrayList<Correo>();
		ClienteEMail clienteGuardado = this.baseDeDatos.get(this.baseDeDatos.indexOf(unCliente));
		if(clienteGuardado.getPassUsuario() == unCliente.getPassUsuario()){
			retorno = unCliente.getInbox();
		}
		return retorno;
	}

	public void conectar(String nombreUsuario, String passusuario) {
		int index = 0;
		while((this.getBaseDeDatos().get(index).getNombreUsuario() != nombreUsuario) && (this.getBaseDeDatos().get(index).getPassUsuario() != passusuario)) {
			index +=1;
		}
		ClienteEMail clienteGuardado = this.baseDeDatos.get(index);
		clienteGuardado.iniciarSesion();
	}
/*
	public void enviar(Correo correo) {
		int index = 0;
      	String destinatario = correo.getDestinatario();
      	while(this.getBaseDeDatos().get(index).getNombreUsuario() != destinatario) {
      		index += 1;
      	}
      	ClienteEMail cliente = this.baseDeDatos.get(index);
      	cliente.agregarCorreo(correo);
	}
*/
	public void enviar(Correo correo) {
		for(ClienteEMail cliente : this.getBaseDeDatos()) {
			if( cliente.getNombreUsuario() == correo.getDestinatario()){
				cliente.agregarCorreo(correo);
			}
		}
	}
	
	@Override
	public float tazaDeTransferencia() {
		//no corresponde
		return 0;
	}

	@Override
	public void resetear() {
         //no corresponde		
	}

	@Override
	public void realizarBackUp() {
		//no corresponde
		
	}



}
