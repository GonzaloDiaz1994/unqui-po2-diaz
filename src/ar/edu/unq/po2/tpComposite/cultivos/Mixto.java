package ar.edu.unq.po2.tpComposite.cultivos;

import java.util.ArrayList;
import java.util.List;

public class Mixto implements ICultivo{
	
	private List<ICultivo> cultivos;
	
	public Mixto() {
		cultivos = new ArrayList<ICultivo>();
	}
	
	private List<ICultivo> getCultivos(){
		return this.cultivos;
	}
	
	public void agregarCultivo(ICultivo cultivo) {
		this.cultivos.add(cultivo);
	}

	public Integer getPrecio() {
		Integer total = 0;
		for(ICultivo cultivo: this.getCultivos()) {
				total += (cultivo.getPrecio()/4);
		}
		return total;
	}
	
}

