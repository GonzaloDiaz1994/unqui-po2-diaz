package ar.edu.unq.po2.tpComposite.shapeShifter;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Contenedor implements IShapeShifter{
	private List<IShapeShifter> contenidos;
	
	public Contenedor() {
		this.setContenidos(new ArrayList<IShapeShifter>());
	}
	
	private void setContenidos(List<IShapeShifter> lista) {
		this.contenidos = lista;
	}
	
	public List<IShapeShifter> getContenidos(){
		return this.contenidos;
	}

	@Override
	public IShapeShifter compose(IShapeShifter iShapeShifter) {
		Contenedor resultado = new Contenedor();
		resultado.addShapeShifter(this);
		resultado.addShapeShifter(iShapeShifter);
		return resultado;
		
	}

	@Override
	public IShapeShifter flat() {
		IShapeShifter resultado = new Contenedor();
		for(IShapeShifter s : this.getContenidos()) {
			this.flatAll(s, resultado);
		}
		return resultado;
	}

	public void flatAll(IShapeShifter shape, IShapeShifter contenedor) {
		for(IShapeShifter contenido : shape.flat().getContenidos()) {
			contenedor.addShapeShifter(contenido);
		}
	}
	
	@Override
	public List<Integer> values() {
		List<Integer> resultado = new ArrayList<Integer>();
		for(IShapeShifter s : this.getContenidos()) {
			resultado.addAll(s.values());
		}
		return resultado;
	}

	@Override
	public Integer deepest() {
		List<Integer> resultado = new ArrayList<Integer>();
		for(IShapeShifter s : this.getContenidos()) {
			resultado.add(s.deepest());
		}
		return Collections.max(resultado) +1;
	}

	@Override
	public void addShapeShifter(IShapeShifter shapeShifter) {
		this.getContenidos().add(shapeShifter);
		
	}
	
}








