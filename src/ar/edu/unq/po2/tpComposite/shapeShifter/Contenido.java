package ar.edu.unq.po2.tpComposite.shapeShifter;

import java.util.ArrayList;
import java.util.List;

public class Contenido implements IShapeShifter{
	
	private Integer valor;
	
	public Contenido(Integer valor) {
		this.setValor(valor);
	}
	
	private void setValor(Integer v) {
		this.valor = v;
	}
	
	public Integer getValor() {
		return this.valor;
	}

	@Override
	public IShapeShifter compose(IShapeShifter shapeShifter) {
		Contenedor resultado = new Contenedor();
		resultado.addShapeShifter(this);
		resultado.addShapeShifter(shapeShifter);
		return resultado;
	}

	@Override
	public IShapeShifter flat() {
		return this;
	}

	@Override
	public List<Integer> values() {
		List<Integer> resultado = new ArrayList<Integer>();
		resultado.add(this.getValor());
		return resultado;
	}

	@Override
	public Integer deepest() {
		return 0;
	}

	@Override
	public List<IShapeShifter> getContenidos() {
		List<IShapeShifter> resultado = new ArrayList<IShapeShifter>();
		resultado.add(this);
		return resultado;
	}

	@Override
	public void addShapeShifter(IShapeShifter shapeShifter2) {
		
	}
}