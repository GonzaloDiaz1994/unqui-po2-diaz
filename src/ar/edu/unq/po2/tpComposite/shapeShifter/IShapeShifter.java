package ar.edu.unq.po2.tpComposite.shapeShifter;

import java.util.List;

public interface IShapeShifter {

	public IShapeShifter compose(IShapeShifter iShapeShifter);
	
	public Integer deepest();
	
	public IShapeShifter flat();
	
	public List<Integer> values();
	
	public List<IShapeShifter> getContenidos();

	public void addShapeShifter(IShapeShifter shapeShifter2);
}
