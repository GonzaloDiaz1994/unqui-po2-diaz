package ar.edu.unq.po2.tpObserver.encuentroDeportivo;

import java.util.ArrayList;
import java.util.List;

public class AplicacionMovil  implements IObservadorInteresado{

	private List<Interes> intereses;
	private int vecesQueRecibioNotificacion;
	
	public AplicacionMovil() {
		this.setIntereses(new ArrayList<Interes>());
	}
	
	public AplicacionMovil(Interes criterios) {
		this.setIntereses(new ArrayList<Interes>());
		this.agregarInteres(criterios);
	}

	public AplicacionMovil(List<Interes> criterios) {
		this.setIntereses(criterios);
	}

	public void subscribirseASistema(Sistema sistema) {
		sistema.agregar(this);
	}
	
	public void agregarInteres(Interes criterio) {
		intereses.add(criterio);
	}
	
	@Override
	public List<Interes> getIntereses() {
		return List.copyOf( intereses ) ;
	}

	private void setIntereses(List<Interes> criteriosDeInteres) {
		this.intereses = criteriosDeInteres;
	}

	@Override
	public void actualizar(EncuentroDeportivo encuentro) {
		vecesQueRecibioNotificacion++;
	}

	public int getVecesQueRecibioNotificacion() {
		return vecesQueRecibioNotificacion;
	}
}
