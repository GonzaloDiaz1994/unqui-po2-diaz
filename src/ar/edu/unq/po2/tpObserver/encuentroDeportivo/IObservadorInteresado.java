package ar.edu.unq.po2.tpObserver.encuentroDeportivo;

import java.util.List;

public interface IObservadorInteresado {

	public List<Interes> getIntereses();

	public void actualizar(EncuentroDeportivo encuentro);

}
