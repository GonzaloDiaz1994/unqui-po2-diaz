package ar.edu.unq.po2.tpObserver.encuentroDeportivo;

import java.util.ArrayList;
import java.util.List;

public abstract class Interes{
	
	private List<Interes> criterios;
	
	public Interes() {
		
	}
	
	public Interes(List<Interes> criterios) {
		this.setCriterios(criterios);
	}

	public final Boolean esEncuentroDeInteres(EncuentroDeportivo encuentro) {
		return this.validarCriterio(encuentro) && 
				this.getCriterios().stream().allMatch(criterio -> criterio.esEncuentroDeInteres(encuentro));
	}

	public abstract Boolean validarCriterio(EncuentroDeportivo encuentro);

	public void agregarCriterio(Interes criterio) {
		this.getCriterios().add(criterio);
	}
	
	private List<Interes> getCriterios() {
		if(criterios == null) {
			this.setCriterios( new ArrayList<Interes>() );
		}
		return criterios;
	}

	private void setCriterios(List<Interes> criterios) {
		this.criterios = criterios;
	}
}
