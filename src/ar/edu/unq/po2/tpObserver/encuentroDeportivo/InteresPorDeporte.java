package ar.edu.unq.po2.tpObserver.encuentroDeportivo;

public class InteresPorDeporte extends Interes {
	public String deporte;
	
	public InteresPorDeporte(String deporte) {
		this.deporte = deporte;
	}
	@Override
	public Boolean validarCriterio(EncuentroDeportivo encuentro) {
		return encuentro.getDeporte() == deporte;
	}

}
