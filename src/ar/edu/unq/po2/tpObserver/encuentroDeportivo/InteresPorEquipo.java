package ar.edu.unq.po2.tpObserver.encuentroDeportivo;

public class InteresPorEquipo extends Interes {
	public String equipo;
	
	public InteresPorEquipo(String equipo) {
		this.equipo = equipo;
	}
	@Override
	public Boolean validarCriterio(EncuentroDeportivo encuentro) {
		return encuentro.tieneContrincante(equipo);
	}

}
