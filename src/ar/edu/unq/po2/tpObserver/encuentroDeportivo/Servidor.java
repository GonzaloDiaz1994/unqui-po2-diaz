package ar.edu.unq.po2.tpObserver.encuentroDeportivo;


import java.util.ArrayList;
import java.util.List;

public class Servidor implements IObservadorInteresado{
	private List<Interes> intereses;
	private int vecesQueRecibioNotificacion;
	
	
	public Servidor() {
		this.setIntereses(new ArrayList<Interes>());
	}
	
	public Servidor(Interes criterios) {
		this.setIntereses(new ArrayList<Interes>());
		this.agregarInteres(criterios);
	}

	public Servidor(List<Interes> criterios) {
		this.setIntereses(criterios);
	}

	public void subscribirseASistema(Sistema sistema) {
		sistema.agregar(this);
	}
		
	public void agregarInteres(Interes criterio) {
		this.getIntereses().add(criterio);
	}
	
	private void setIntereses(List<Interes> criteriosDeInteres) {
		this.intereses = criteriosDeInteres;
	}

	public int getVecesQueRecibioNotificacion() {
		return vecesQueRecibioNotificacion;
	}

	@Override
	public List<Interes> getIntereses() {
		return this.intereses;
	}

	@Override
	public void actualizar(EncuentroDeportivo encuentro) {
		vecesQueRecibioNotificacion++;
	}
}
