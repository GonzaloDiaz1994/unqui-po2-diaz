package ar.edu.unq.po2.tpObserver.encuentroDeportivo;

import java.util.ArrayList;
import java.util.List;


public class Sistema {

	private List<IObservadorInteresado> observadores;
	private List<EncuentroDeportivo> encuentros;
	
	public Sistema() {
		this.setObservadores(new ArrayList<IObservadorInteresado>());
		this.setEncuentros(new ArrayList<EncuentroDeportivo>());
	}	
	
	public void agregar(IObservadorInteresado observador) {
		this.getObservadores().add(observador);
	}
	
	public void quitar(IObservadorInteresado observador) {
		this.getObservadores().remove(observador);
	}

	public void notificar(EncuentroDeportivo publicacion) {
		this.getObservadores().stream()
		.filter(
			observador -> this.esPublicacionDeInteres(observador, publicacion)
		)
		.forEach(
			observador -> observador.actualizar(publicacion)
		);
	}
	
	private Boolean esPublicacionDeInteres(IObservadorInteresado observador, EncuentroDeportivo encuentro) {
		return observador.getIntereses().stream().anyMatch( criterio -> criterio.esEncuentroDeInteres(encuentro) );
	}	
	
	public void agregarEncuentro(EncuentroDeportivo encuentro) {
		this.getEncuentros().add(encuentro);
		this.notificar(encuentro);
	}
	
	private List<IObservadorInteresado> getObservadores() {
		return observadores;
	}

	private void setObservadores(List<IObservadorInteresado> investigadoresObservers) {
		this.observadores = investigadoresObservers;
	}
	
	private List<EncuentroDeportivo> getEncuentros() {
		return encuentros;
	}
	
	private void setEncuentros(List<EncuentroDeportivo> encuentros) {
		this.encuentros = encuentros;
	}
}
