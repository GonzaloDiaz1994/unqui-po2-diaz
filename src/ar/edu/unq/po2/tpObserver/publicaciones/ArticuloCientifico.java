package ar.edu.unq.po2.tpObserver.publicaciones;

import java.util.List;

public class ArticuloCientifico {
	private String titulo;
	private List<String> autores;
	private String filiacion;
	private String tipoArticulo;
	private String lugarPublicacion;
	private List<String> palabrasClaves;
	
	public ArticuloCientifico(String titulo, List<String> autores, String filiaciones,
			String tipoArticulo, String lugarPublicacion, List<String> palabrasClaves) {
		this.setTitulo(titulo);
		this.setAutores(autores);
		this.setFiliacion(filiaciones);
		this.setTipoArticulo(tipoArticulo);
		this.setLugarPublicacion(lugarPublicacion);
		this.setPalabrasClaves(palabrasClaves);
		
	}

	public String getTitulo() {
		return titulo;
	}

	private void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<String> getAutores() {
		return autores;
	}

	private void setAutores(List<String> autores) {
		this.autores = autores;
	}

	public String getFiliacion() {
		return filiacion;
	}

	private void setFiliacion(String filiaciones) {
		this.filiacion = filiaciones;
	}

	public String getTipoArticulo() {
		return tipoArticulo;
	}

	private void setTipoArticulo(String tipoArticulo) {
		this.tipoArticulo = tipoArticulo;
	}

	public String getLugarPublicacion() {
		return lugarPublicacion;
	}

	private void setLugarPublicacion(String lugarPublicacion) {
		this.lugarPublicacion = lugarPublicacion;
	}

	public List<String> getPalabrasClaves() {
		return palabrasClaves;
	}

	private void setPalabrasClaves(List<String> palabrasClaves) {
		this.palabrasClaves = palabrasClaves;
	}

	
	
	
}
