package ar.edu.unq.po2.tpObserver.publicaciones;

public interface ISuscriptor {
	
	public boolean estaInteresadoEn(ArticuloCientifico articulo);
	
	public void recibirNotificacion(ArticuloCientifico unArticulo);

	public void addInteres(Interes unInteres);
	
}