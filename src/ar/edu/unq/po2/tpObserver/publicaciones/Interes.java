package ar.edu.unq.po2.tpObserver.publicaciones;

public abstract class Interes {

	private String interes;
	
	public Interes(String interes) {
		this.setInteres(interes);
	}
	
	private void setInteres(String i) {
		this.interes = i;
	}
	
	public String getInteres() {
		return this.interes;
	}
	
	public abstract Boolean conincideCon(ArticuloCientifico unArticulo);
	
}
