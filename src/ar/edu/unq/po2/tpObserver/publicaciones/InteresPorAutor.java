package ar.edu.unq.po2.tpObserver.publicaciones;

public class InteresPorAutor extends Interes{

	public InteresPorAutor(String interes) {
		super(interes);
	}

	@Override
	public Boolean conincideCon(ArticuloCientifico unArticulo) {
		Boolean resultado = false;
		for(String autor: unArticulo.getAutores()) {
			resultado = resultado || this.getInteres() == autor;
		}
		return resultado;
	}

	
}
