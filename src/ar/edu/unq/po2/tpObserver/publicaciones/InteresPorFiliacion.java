package ar.edu.unq.po2.tpObserver.publicaciones;

public class InteresPorFiliacion extends Interes{

	public InteresPorFiliacion(String interes) {
		super(interes);
	}

	@Override
	public Boolean conincideCon(ArticuloCientifico unArticulo) {
		return this.getInteres() == unArticulo.getFiliacion();
	}

}
