package ar.edu.unq.po2.tpObserver.publicaciones;

public class InteresPorLugarDePublicacion extends Interes{

	public InteresPorLugarDePublicacion(String interes) {
		super(interes);
	}

	@Override
	public Boolean conincideCon(ArticuloCientifico unArticulo) {
		return this.getInteres() == unArticulo.getLugarPublicacion();
	}

}
