package ar.edu.unq.po2.tpObserver.publicaciones;

public class InteresPorPalabraClave extends Interes {

	public InteresPorPalabraClave(String interes) {
		super(interes);
	}

	@Override
	public Boolean conincideCon(ArticuloCientifico unArticulo) {
		Boolean resultado = false;
		for(String palabra : unArticulo.getPalabrasClaves()) {
			resultado = resultado || this.getInteres() == palabra;
		}
		return resultado;
	}

}
