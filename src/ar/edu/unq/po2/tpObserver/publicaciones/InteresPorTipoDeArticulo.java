package ar.edu.unq.po2.tpObserver.publicaciones;


public class InteresPorTipoDeArticulo extends Interes {

	public InteresPorTipoDeArticulo(String interes) {
		super(interes);
	}

	@Override
	public Boolean conincideCon(ArticuloCientifico unArticulo) {
		return this.getInteres() == unArticulo.getTipoArticulo();
	}

}
