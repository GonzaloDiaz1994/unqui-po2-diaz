package ar.edu.unq.po2.tpObserver.publicaciones;


public class InteresPorTitulo extends Interes {

	public InteresPorTitulo(String interes) {
		super(interes);
	}

	@Override
	public Boolean conincideCon(ArticuloCientifico unArticulo) {
		return this.getInteres() == unArticulo.getTitulo();
	}

}
