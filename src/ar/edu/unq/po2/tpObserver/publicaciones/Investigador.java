package ar.edu.unq.po2.tpObserver.publicaciones;

import java.util.ArrayList;
import java.util.List;

public class Investigador implements ISuscriptor{

	private List<Interes> intereses; 
	private List<ArticuloCientifico> misArticulos;
	
	public Investigador() {
		this.setIntereses(new ArrayList<Interes>());
		this.setMisArticulos(new ArrayList<ArticuloCientifico>());
	}
	
	private void setIntereses(List<Interes> lista) {
		this.intereses = lista;
	}
	
	public List<Interes> getIntereses(){
		return this.intereses;
	}
	
	private void setMisArticulos(List<ArticuloCientifico> lista) {
		this.misArticulos = lista;
	}
	
	public List<ArticuloCientifico> getMisArticulos(){
		return this.misArticulos;
	}
	
	@Override
	public boolean estaInteresadoEn(ArticuloCientifico unArticulo) {
		Boolean resultado = false;
		for(Interes i: this.getIntereses()) {
			resultado = resultado || i.conincideCon(unArticulo); 
		}
		return resultado;
	}

	@Override
	public void recibirNotificacion(ArticuloCientifico unArticulo) {
		this.getMisArticulos().add(unArticulo);
	}

	@Override
	public void addInteres(Interes unInteres) {
		this.getIntereses().add(unInteres);
	}
}



















