package ar.edu.unq.po2.tpObserver.publicaciones;

import java.util.ArrayList;
import java.util.List;

public class Sistema {
	private List<ArticuloCientifico> publicaciones;
	private List<ISuscriptor> suscriptores;
	
	public Sistema() {
		this.publicaciones = new ArrayList<ArticuloCientifico>();
		this.suscriptores = new ArrayList<ISuscriptor>();
	}
	
	public void updatePublicaciones(ArticuloCientifico unArticulo){
		this.getPublicaciones().add(unArticulo);
		this.notificarSuscriptores(unArticulo);
	}

	protected List<ArticuloCientifico> getPublicaciones() {
		return this.publicaciones;
	}
	
	protected List<ISuscriptor> getSuscriptores(){
		return this.suscriptores;
	}
	
	protected void agregarSuscriptor(ISuscriptor sus) {
		this.getSuscriptores().add(sus);
	}
	
	protected void notificarSuscriptores(ArticuloCientifico unArticulo) {
		for(ISuscriptor sus: this.getSuscriptores()) {
			if(sus.estaInteresadoEn(unArticulo)) {
				sus.recibirNotificacion(unArticulo);
			}
		}
	}
	
}
