package ar.edu.unq.po2.tpRefactoring.cuenta;

public abstract class CuentaBancaria {
	protected HistorialMovimientos historialDeMovimientos;
	protected Notificador notificador;
	protected int saldo;
	
	public CuentaBancaria(HistorialMovimientos historialDeMovimientos, Notificador notificador, Integer saldo) {
		super();
		this.setHistorialDeMovimientos(historialDeMovimientos);
		this.setNotificador(notificador);
		this.setSaldo(saldo);
	}
	
	public void setHistorialDeMovimientos(HistorialMovimientos historialDeMovimientos) {
		this.historialDeMovimientos = historialDeMovimientos;
	}

	public void setNotificador(Notificador notificador) {
		this.notificador = notificador;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public int getSaldo() {
		return saldo;
	}
	
	public void extraer(Integer monto) {
		if(this.condicion(monto)) {
			this.saldo = saldo - monto;
			this.historialDeMovimientos.registrarMovimiento("Extracción", monto);
			this.notificador.notificarNuevoSaldoACliente(this);
		}
	}
	
	protected abstract Boolean condicion(Integer monto);
}
