package ar.edu.unq.po2.tpRefactoring.cuenta;

public interface HistorialMovimientos {

	public void registrarMovimiento(String descripcion, Integer monto);

}
