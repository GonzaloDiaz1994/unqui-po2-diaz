package ar.edu.unq.po2.tpRefactoring.cuenta;

public interface Notificador {
	
	public void notificarNuevoSaldoACliente(CuentaBancaria cuentaBancaria);

}
