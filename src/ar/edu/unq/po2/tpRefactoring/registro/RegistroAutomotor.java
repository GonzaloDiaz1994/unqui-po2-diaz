package ar.edu.unq.po2.tpRefactoring.registro;

public class RegistroAutomotor {

	public Boolean debeRealizarVtv(Vehiculo unVehiculo) {
		
		return !unVehiculo.esVehiculoPolicial() && 
				unVehiculo.tieneUnAnioDeAntiguedad() && 
				unVehiculo.esDeBuenosAires();
	}
	
}
