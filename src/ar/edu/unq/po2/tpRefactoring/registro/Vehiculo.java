package ar.edu.unq.po2.tpRefactoring.registro;

import java.time.LocalDate;

public class Vehiculo {
	private Boolean esVehiculoPolicial;
	private LocalDate fechaFabricacion;	
	private String ciudadRadicacion; 
	
	public Vehiculo(Boolean esVehiculoPolicial, LocalDate fechaFabricacion, String ciudadRadicacion) {
		this.setEsVehiculoPolicial(esVehiculoPolicial);
		this.setFechaFabricacion(fechaFabricacion);
		this.setCiudadRadicacion(ciudadRadicacion);
	}

	private void setEsVehiculoPolicial(Boolean esVehiculoPolicial) {
		this.esVehiculoPolicial = esVehiculoPolicial;
	}

	private void setFechaFabricacion(LocalDate fechaFabricacion) {
		this.fechaFabricacion = fechaFabricacion;
	}

	private void setCiudadRadicacion(String ciudadRadicacion) {
		this.ciudadRadicacion = ciudadRadicacion;
	}
	
	private Boolean getEsVehiculoPolicial() {
		return this.esVehiculoPolicial;
	}

	public String getCiudadRadicacion() {
		return this.ciudadRadicacion;
	}

	public LocalDate getFechaFabricacion() {
		return this.fechaFabricacion;
	}
	
	public Boolean esVehiculoPolicial() {
		return this.getEsVehiculoPolicial();
	}

	public Boolean tieneUnAnioDeAntiguedad() {
		LocalDate hoy = LocalDate.now();
		return hoy.minusYears(1).isAfter(this.getFechaFabricacion());
	}
	
	public Boolean esDeBuenosAires() {
		return this.getCiudadRadicacion() == "Buenos Aires";
	}
}
