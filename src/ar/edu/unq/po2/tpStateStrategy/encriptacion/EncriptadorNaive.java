package ar.edu.unq.po2.tpStateStrategy.encriptacion;

public class EncriptadorNaive {
	
	private IEstrategiaEncriptado estrategiaEncriptado;

	public EncriptadorNaive() {
		this.setEstrategiaEncriptado(new EstrategiaCambiarOrden());
		
	}
	
	public void setEstrategiaEncriptado(IEstrategiaEncriptado unaEstrategia) {
		this.estrategiaEncriptado = unaEstrategia;
	}
	
	public IEstrategiaEncriptado getEstrategiaEncriptado() {
		return this.estrategiaEncriptado;
	}
	
	public String encriptar(String texto) {
		return this.getEstrategiaEncriptado().encriptar(texto);
	}
	
	public String desencriptar(String texto) {
		return this.getEstrategiaEncriptado().desencriptar(texto);
	}
	
	
}
