package ar.edu.unq.po2.tpStateStrategy.encriptacion;


public class EstrategiaCambiarOrden implements IEstrategiaEncriptado{

	@Override
	public String encriptar(String texto) {
		String resultado = ("");
		int longitud = texto.length();
		for (int x = longitud -1 ; x>=0 ; x--)
			  resultado = resultado + (texto.charAt(x));
		return resultado;
	}
	
	

	@Override
	public String desencriptar(String texto) {
		String resultado = ("");
		int longitud = texto.length();
		for (int x = longitud -1 ; x>=0 ; x--)
			  resultado = resultado + (texto.charAt(x));
		return resultado;
	}

}
