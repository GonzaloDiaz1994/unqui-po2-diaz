package ar.edu.unq.po2.tpStateStrategy.encriptacion;

import java.util.HashMap;
import java.util.Map;

public class EstrategiaLetraPorNumero implements IEstrategiaEncriptado{

	@Override
	public String encriptar(String texto) {
		String resultado = ("");
		String[] palabra = texto.split("");
		for(String letra : palabra) {
			resultado += this.abecedarioEnNumero().get(letra) + ",";
		}
		return resultado;
	}

	public Map<String, Integer> abecedarioEnNumero(){
		String abecedario = " abcdefghijklmnopqrstuvwxyz";
		String[] texto = abecedario.split("");
		Integer contador = 0;
		Map<String, Integer> diccionario = new HashMap<String, Integer>();
		for(String letra : texto) {
			diccionario.put(letra, contador);
			contador += 1;
		}
		return diccionario;
	}
	
	@Override
	public String desencriptar(String texto) {
		return "no se hacerlo";
	}

}
