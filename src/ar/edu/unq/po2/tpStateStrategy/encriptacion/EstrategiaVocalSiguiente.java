package ar.edu.unq.po2.tpStateStrategy.encriptacion;

public class EstrategiaVocalSiguiente implements IEstrategiaEncriptado{
		
	@Override
	public String encriptar(String texto) {
		String resultado = ("");
		char[] letras = texto.toCharArray();
		for(char letra: letras) {
			if(letra == 'a') {
				resultado += 'e';
			}
			else if(letra == 'e') {
				resultado += 'i';
			}
			else if(letra == 'i') {
				resultado += 'o';
			}
			
			else if(letra == 'o') {
				resultado += 'u';
			}
			else if(letra == 'u') {
				resultado += 'a';
			}
			
			else {
				resultado += letra;
			}
		}
		return resultado;
	}
	

	@Override
	public String desencriptar(String texto) {
		String resultado = ("");
		char[] letras = texto.toCharArray();
		for(char letra: letras) {
			if(letra == 'a') {
				resultado += 'u';
			}
			else if(letra == 'e') {
				resultado += 'a';
			}
			else if(letra == 'i') {
				resultado += 'e';
			}
			else if(letra == 'o') {
				resultado += 'i';
			}
			else if(letra == 'u') {
				resultado += 'o';
			}
			else {
				resultado += letra;
			}
		}
		return resultado;
	}
	
}

/*
	@Override
	public String encriptar(String texto) {
	String resultado = "";
	char[] letras = texto.toCharArray();
	for(char letra: letras) {
		resultado += this.vocalSiguiente(letra);
		}
		return resultado;
	}

	public char vocalSiguiente(char c) {
	char resultado = c;
	switch(c) {
		case 'a':
			resultado += 'e';
			break;
		case 'e':
			resultado += 'i';
			break;
		case 'i':
			resultado += 'o';
			break;
		case 'o':
			resultado += 'u';
			break;
		case 'u':
			resultado += 'a';
			break;
		}
		return resultado;
	}

	@Override
	public String desencriptar(String texto) {
	String resultado = "";
	char[] letras = texto.toCharArray();
	for(char letra: letras) {
		resultado +=this.vocalAnterior(letra);
		}
		return resultado;
	}

	public char vocalAnterior(char c) {
	char resultado = c;
	
	switch(c) {
		case 'a':
			resultado += 'u';
			break;
		case 'e':
			resultado += 'a';
			break;
		case 'i':
			resultado += 'e';
			break;
		case 'o':
			resultado += 'i';
			break;
		case 'u':
			resultado += 'o';
			break;
		}
		return resultado;
	}
}
*/
