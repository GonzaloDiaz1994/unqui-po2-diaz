package ar.edu.unq.po2.tpStateStrategy.encriptacion;

public interface IEstrategiaEncriptado {

	public String encriptar(String texto);
	
	public String desencriptar(String texto);
}
