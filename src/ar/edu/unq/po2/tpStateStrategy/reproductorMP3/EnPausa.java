package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

public class EnPausa implements StateSong {

	@Override
	public void play(Song unaCancion) {
		unaCancion.cambiarEstado(new EnReproduccion());
	}

	@Override
	public void pause(Song unaCancion) {
		unaCancion.cambiarEstado(new EnReproduccion());
	}

	@Override
	public void stop(Song unaCancion) {
		unaCancion.cambiarEstado(new EnStop());
	}

	@Override
	public Boolean estaEnReproduccion() {
		return false;
	}

}
