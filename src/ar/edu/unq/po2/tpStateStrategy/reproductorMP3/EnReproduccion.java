package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

public class EnReproduccion implements StateSong{

	@Override
	public void play(Song unaCancion) {
	}

	@Override
	public void pause(Song unaCancion) {
		unaCancion.cambiarEstado(new EnPausa());
	}

	@Override
	public void stop(Song unaCancion) {
		unaCancion.cambiarEstado(new EnStop());
	}

	@Override
	public Boolean estaEnReproduccion() {
		return true;
	}

}
