package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

public class Reproduciendo implements StateReproductor{

	@Override
	public void play(Reproductor unReproductor, Song unaCancion) throws Exception{
		throw new Exception("Ya est� reproduciendo");
	}

	@Override
	public void pause(Reproductor unReproductor, Song unaCancion){
		if(unaCancion.getEstado().estaEnReproduccion()) {
			unaCancion.pause();
		}else {
			unaCancion.play();
		}
	}

	@Override
	public void stop(Reproductor unReproductor, Song unaCancion) {
		unaCancion.stop();
		unReproductor.cambiarEstado(new SeleccionadorDeCanciones());
	}

	@Override
	public Boolean estaReproduciendo() {
		return true;
	}

}
