package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

import java.util.ArrayList;
import java.util.List;

public class Reproductor {

	private StateReproductor estado;
	private List<Song> canciones;
	private Song cancionSeleccionada;
	
	public Reproductor() {
		this.setEstado(new SeleccionadorDeCanciones());
		this.setCanciones(new ArrayList<Song>());
		this.setCancionSeleccionada(null);
	}
	
	private void setCancionSeleccionada(Song cancion) {
		this.cancionSeleccionada = cancion;
	}
	
	public Song getCancionSeleccionada() {
		return this.cancionSeleccionada;
	}
	
	private void setEstado(StateReproductor unEstado) {
		this.estado = unEstado;
	}
	
	public StateReproductor getEstado() {
		return this.estado;
	}
	
	private void setCanciones(List<Song> unaLista) {
		this.canciones = unaLista;
	}
	
	public List<Song> getCanciones(){
		return this.canciones;
	}
	
	public void agregarCancion(Song cancion) {
		this.getCanciones().add(cancion);
	}
	
	public void seleccionarCancion(Song unaCancion) {
		for(Song cancion: this.getCanciones()) {
			if(cancion.getNombre() == unaCancion.getNombre()) {
				this.setCancionSeleccionada(cancion);
			}
		}
	}
	
	public void cambiarEstado(StateReproductor unEstado) {
		this.setEstado(unEstado);
		
	}
	
	public void play() throws Exception {
		try{
			this.getEstado().play(this, this.getCancionSeleccionada());
		}catch (Exception e) {
			throw e;
		}
	}
	
	public void pause() throws Exception{
		try {
			this.getEstado().pause(this, this.getCancionSeleccionada());
		}catch (Exception e) {
			throw e;
		}
	}
	
	public void stop() {
		this.getEstado().stop(this, this.getCancionSeleccionada());
	}
	
}
