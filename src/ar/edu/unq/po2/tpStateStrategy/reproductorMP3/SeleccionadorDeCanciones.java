package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

public class SeleccionadorDeCanciones implements StateReproductor {

	@Override
	public void play(Reproductor unReproductor, Song unaCancion) {
		unaCancion.play();
		unReproductor.cambiarEstado(new Reproduciendo());
	}

	@Override
	public void pause(Reproductor unReproductor, Song unaCancion) throws Exception{
		throw new Exception("No hay nada para pausar");
	}

	@Override
	public void stop(Reproductor unReproductor, Song unaCancion) {
		
	}

	@Override
	public Boolean estaReproduciendo() {
		return false;
	}

}
