package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

public class Song {

	private String nombre;
	private StateSong estado;
	
	public Song(String nombre) {
		this.setNombre(nombre);
		this.setEstado(new EnStop());
	}
	
	private void setNombre(String unNombre) {
		this.nombre = unNombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	private void setEstado(StateSong unEstado) {
		this.estado = unEstado;
	}
	
	public StateSong getEstado() {
		return this.estado;
	}

	public void cambiarEstado(StateSong estado) {
		this.setEstado(estado);
	}
	
	public void play() {
		this.getEstado().play(this);
	}
	
	public void pause() {
		this.getEstado().pause(this);;
	}
	
	public void stop() {
		this.getEstado().stop(this);;
	}
	
	public Boolean estaEnReproduccion() {
		return this.getEstado().estaEnReproduccion();
	}
}
