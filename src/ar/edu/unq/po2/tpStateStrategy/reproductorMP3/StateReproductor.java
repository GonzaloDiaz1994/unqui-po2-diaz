package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

public interface StateReproductor {	

	public void play(Reproductor unReproductor, Song unaCancion) throws Exception;
	
	public void pause(Reproductor unReproductor, Song unaCancion) throws Exception;
	
	public void stop(Reproductor unReproductor, Song unaCancion);

	public Boolean estaReproduciendo();
}
