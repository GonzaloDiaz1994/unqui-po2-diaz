package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

public interface StateSong {

	public void play(Song unaCancion);
	
	public void pause(Song unaCancion);
	
	public void stop(Song unaCancion);
	
	public Boolean estaEnReproduccion();
}
