package ar.edu.unq.po2.tpStateStrategy.videoJuego;

public class BotonInicioConDosMonedas extends BotonInicio {

	@Override
	public String presionarBoton() {
		return "Comienza juego para dos jugadores";
	}

}
