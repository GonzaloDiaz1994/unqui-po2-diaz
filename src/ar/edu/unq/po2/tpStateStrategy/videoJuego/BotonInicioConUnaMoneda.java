package ar.edu.unq.po2.tpStateStrategy.videoJuego;

public class BotonInicioConUnaMoneda extends BotonInicio {

	@Override
	public String presionarBoton() {
		return "Comienza juego para un solo jugador";
	}

}
