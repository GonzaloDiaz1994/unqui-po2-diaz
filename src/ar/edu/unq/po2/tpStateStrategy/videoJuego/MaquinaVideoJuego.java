package ar.edu.unq.po2.tpStateStrategy.videoJuego;

public class MaquinaVideoJuego {

	private BotonInicio estado;
	private Integer cantidadMonedas;
	
	public MaquinaVideoJuego() {
		this.setEstado(new BotonInicioSinMonedas());
		this.setCantidadMonedas(0);
		
	}

	private void setCantidadMonedas(int i) {
		this.cantidadMonedas = i;
	}

	public Integer getCantidadMonedas() {
		return this.cantidadMonedas;
	}

	private void setEstado(BotonInicio boton) {
		this.estado = boton;	
	}
	
	public BotonInicio getEstado() {
		return this.estado;
	}

	public void ingresarMoneda() {
		this.cantidadMonedas +=1;
		this.setEstado(new BotonInicioConUnaMoneda());
		if(this.getCantidadMonedas() >= 2) {
			this.setEstado(new BotonInicioConDosMonedas());
		}
	}
	
	public String encenderMaquina() {
		return this.estado.presionarBoton();
	}
	
	public void terminoElJuego() {
		this.setCantidadMonedas(0);
		this.setEstado(new BotonInicioSinMonedas());
		
	}
	
	
}
