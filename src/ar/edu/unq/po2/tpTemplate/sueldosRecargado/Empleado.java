package ar.edu.unq.po2.tpTemplate.sueldosRecargado;

public abstract class Empleado {
	private Integer horasTrabajadas;
	private Integer hijos;
	private Boolean casado;

	public Empleado(Integer horas, Integer hijos, Boolean casado) {
		this.horasTrabajadas = horas;
		this.hijos = hijos;
		this.casado = casado;
	}
	
	public Empleado(Integer cantidad) {
	}

	protected Integer getHorasTrabajadas() {
		return this.horasTrabajadas;
	}
	
	protected void setHorasTrabajadas(Integer horas) {
		this.horasTrabajadas = horas;
	}
	
	protected void setHijos(Integer hijos) {
		this.hijos = hijos;
	}
	
	protected Integer getHijos() {
		return this.hijos;
	}
	
	protected Boolean getCasado() {
		return this.casado;
	}

	public final Double sueldo() {
		Double total = 0d;
		total += this.pagoPorHorasTrabajadas() +
				 this.pagoPorHijos() +
				 this.pagoPorConcubino() +
				 this.sueldoBasico();
		return total - (total * this.porcentajeDescuento(13));
	}
	
	private Double porcentajeDescuento(Integer descuento) {
		return descuento / 100d;
	}
	
	protected Integer sueldoBasico() {
		return 0;
	}
	
	protected Integer pagoPorHorasTrabajadas() {
		return 0;
	}
	
	protected Integer pagoPorHijos() {
		return 0;
	}
	
	protected Integer pagoPorConcubino() {
		return 0;
	}
}
