package ar.edu.unq.po2.tpTemplate.sueldosRecargado;


public class EmpleadoDePlanta extends Empleado {

	public EmpleadoDePlanta(Integer horas, Integer hijos, Boolean casado) {
		super(horas, hijos, casado);
	}
	
	public EmpleadoDePlanta(Integer hijos) {
		super(hijos);
		this.setHijos(hijos);
	}
	
	@Override
	protected Integer sueldoBasico() {
		return 3000;
	}

	@Override
	protected Integer pagoPorHijos() {
		return 150 * this.getHijos();
	}

}
