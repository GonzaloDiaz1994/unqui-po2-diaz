package ar.edu.unq.po2.tpTemplate.sueldosRecargado;



public class EmpleadoPasante extends Empleado {

	public EmpleadoPasante(Integer horas, Integer hijos, Boolean casado) {
		super(horas, hijos, casado);		
	}
	
	public EmpleadoPasante(Integer horas) {
		super(horas);
		this.setHorasTrabajadas(horas);
	}

	@Override
	protected Integer pagoPorHorasTrabajadas() {
		return 40 * this.getHorasTrabajadas();
	}

}
