package ar.edu.unq.po2.tpTemplate.sueldosRecargado;


public class EmpleadoTemporario extends Empleado {

	public EmpleadoTemporario(Integer horas, Integer hijos, Boolean casado) {
		super(horas, hijos, casado);
	}

	@Override
	protected Integer pagoPorHorasTrabajadas() {
		return 5 * this.getHorasTrabajadas();
	}

	@Override
	protected Integer pagoPorHijos() {
		if(this.getHijos()>0 || this.getCasado()) {
			return 100;
		}else {
			return 0;
		}
	}

	@Override
	protected Integer sueldoBasico() {
		return 1000;
	}

}
