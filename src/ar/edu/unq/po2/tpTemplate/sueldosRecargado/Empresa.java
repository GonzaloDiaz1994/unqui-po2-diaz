package ar.edu.unq.po2.tpTemplate.sueldosRecargado;

import java.util.ArrayList;
import java.util.List;

public class Empresa {
	
	private List<Empleado> empleados;
		
	public Empresa() {
		this.empleados = new ArrayList<Empleado>();
	}
	
	public void agregarEmpleado(Empleado unEmpleado) {
		this.empleados.add(unEmpleado);
	}

	private List<Empleado> getEmpleados(){
		return this.empleados;
	}
	
	public Double pagarSueldos() {
		Double total = 0d;
		for(Empleado empleado : this.getEmpleados()) {
			total += empleado.sueldo();
		}
		return total;
	}
}
