package ar.edu.unq.po2.tpTemplate.wikipedia;

import java.util.ArrayList;
import java.util.List;

public abstract class Filtro {
	

	public Filtro() {
	}
	
	protected void agregarPaginaALista (WikipediaPage page, List<WikipediaPage> wikipedia) {
		wikipedia.add(page);
	}
	
	public final List<WikipediaPage> getSimilarPages(WikipediaPage page, List<WikipediaPage> wikipedia) {
		List<WikipediaPage> wiki = new ArrayList<WikipediaPage>();
		for(WikipediaPage pagina : wikipedia) {
			if(this.condicion(page, pagina)) {
				this.agregarPaginaALista(pagina, wiki);
			}
		}
		return wiki;
	}
	
	protected abstract Boolean condicion(WikipediaPage page, WikipediaPage pagina);

}