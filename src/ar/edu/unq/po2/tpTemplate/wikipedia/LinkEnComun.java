package ar.edu.unq.po2.tpTemplate.wikipedia;

import java.util.ArrayList;
import java.util.List;

public class LinkEnComun extends Filtro {

	public LinkEnComun() {
	}

	@Override
	protected Boolean condicion(WikipediaPage page, WikipediaPage pagina) {
		List<Boolean> valores = new ArrayList<Boolean>();
		for(WikipediaPage link : page.getLinks()) {
			valores.add(pagina.getLinks().contains(link));
		}
		return valores.contains(true);
	}


}
