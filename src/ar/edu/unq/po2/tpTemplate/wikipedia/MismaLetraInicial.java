package ar.edu.unq.po2.tpTemplate.wikipedia;

public class MismaLetraInicial extends Filtro {

	public MismaLetraInicial() {
	}

	protected char primerLetra(WikipediaPage page) {
		return page.getTittle().charAt(0);
	}

	@Override
	protected Boolean condicion(WikipediaPage page, WikipediaPage pagina) {
		return (this.primerLetra(page) == this.primerLetra(pagina));
	}

}
