package ar.edu.unq.po2.tpTemplate.wikipedia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaginaDeWikipedia implements WikipediaPage {
	private String tittle;
	private List<WikipediaPage> links;
	private Map<String, WikipediaPage> infobox;

	public PaginaDeWikipedia(String titulo) {
		this.tittle = titulo;
		this.links = new ArrayList<WikipediaPage>();
		this.infobox = new HashMap<String, WikipediaPage>();
	}
	
	@Override
	public String getTittle() {
		return this.tittle;
	}

	@Override
	public List<WikipediaPage> getLinks() {
		return this.links;
	}

	@Override
	public Map<String, WikipediaPage> getInfobox() {
		return this.infobox;
	}

	public void agregarLink(WikipediaPage unLink) {
		this.links.add(unLink);
	}
	
	public void agregarInfobox(String key, WikipediaPage page){
		this.infobox.put(key, page);
	}

}
