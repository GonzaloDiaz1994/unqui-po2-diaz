package ar.edu.unq.po2.tpTemplate.wikipedia;

import java.util.ArrayList;
import java.util.List;

public class PropiedadEnComun extends Filtro {

	public PropiedadEnComun() {
	}

	@Override
	protected Boolean condicion(WikipediaPage page, WikipediaPage pagina) {
		List<Boolean> valores = new ArrayList<Boolean>();
		for(String key : this.allKeys(page)) {
			valores.add(pagina.getInfobox().containsKey(key));
		}
		return valores.contains(true);
	}

	private List<String> allKeys(WikipediaPage page){
		List<String> keys = new ArrayList<String>();
		for(String key : page.getInfobox().keySet()) {
			keys.add(key);
		}
		return keys;
		
	}
}
