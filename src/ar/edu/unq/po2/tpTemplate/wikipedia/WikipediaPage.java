package ar.edu.unq.po2.tpTemplate.wikipedia;

import java.util.List;
import java.util.Map;

public interface WikipediaPage {

	public String getTittle(); /*retorna el titulo de la pagina*/
	
	public List<WikipediaPage> getLinks(); /* retorna una lista de las paginas de wikipedia con las que se conecta*/
	
	public Map<String, WikipediaPage> getInfobox(); /* retorna un Map con un valor en el texto y la pagina que describe
	ese valor que aparececn en los infobox de la pagina de wikipedia*/
	
	
}
