package ar.edu.unq.po2.tpTestDoubles;

public class Carta {

	private Integer valor;
	private String palo;
	
	public Carta(Integer valor, String palo) {
		this.valor = valor;
		this.palo = palo;
	}
	
	public Integer getValor() {
		return valor;
	}
	
	public String getPalo() {
		return palo;
	}
	
	public Boolean esMayorA(Carta unaCarta) {
		return this.getValor() > unaCarta.getValor();
	}
	
	public Boolean esMismoPaloA(Carta unaCarta) {
		return this.getPalo() == unaCarta.getPalo();
	}
}
