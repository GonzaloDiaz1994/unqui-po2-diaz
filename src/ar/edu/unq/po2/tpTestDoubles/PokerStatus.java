package ar.edu.unq.po2.tpTestDoubles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PokerStatus {
	private List<Carta> cartas;
	
	
	private int pica;
	private int corazones;
	private int diamantes;
	private int trebol;
	
	public PokerStatus(List<Carta> cartas2) {
		this.setCartas(cartas2);
		this.setPica(0);
		this.setCorazones(0);
		this.setDiamantes(0);
		this.setTrebol(0);
	}
		
	private void setCartas(List<Carta> cartas) {
		this.cartas = cartas;
	}
	
	private List<Carta> getCartas(){
		return this.cartas;
	}
	
	
	public int getPica() {
		return pica;
	}

	public void setPica(int pica) {
		this.pica = pica;
	}

	public int getCorazones() {
		return corazones;
	}

	public void setCorazones(int corazones) {
		this.corazones = corazones;
	}

	public int getDiamantes() {
		return diamantes;
	}

	public void setDiamantes(int diamantes) {
		this.diamantes = diamantes;
	}

	public int getTrebol() {
		return trebol;
	}

	public void setTrebol(int trebol) {
		this.trebol = trebol;
	}

	private boolean esPica(Carta carta) {
		return carta.getPalo() == "Pica";
	}
	
	private boolean esCorazon(Carta carta) {
		return carta.getPalo() == "Corazones";
	}
	
	private boolean esDiamante(Carta carta) {
		return carta.getPalo() == "Diamante";
	}
	
	private boolean esTrebol(Carta carta) {
		return carta.getPalo() == "Trebol";
	}

	public void limpiar() {
		this.setPica(0);
		this.setCorazones(0);
		this.setDiamantes(0);
		this.setTrebol(0);
	}
	private boolean hayPoker(List<Integer> cantidades) {
		return cantidades.contains(4);
	}

	private boolean hayColor(List<Integer> cantidades) {
		return cantidades.contains(5);
	}
	
	public String verificar() {
		if(this.esColor()) {
			return "Color";
		}
		this.limpiar();
		if(this.esPoker()) {
			return "Poquer";
		}
		this.limpiar();
		if(this.esTrio()) {
			return "Trio";
		}
		else {
			return "Nada";
		}
	}
	
	public boolean esPoker() {
		List<Integer> cantidades = new ArrayList<Integer>();
		this.operacionSobrePalos();
		cantidades = this.cantidades(pica, corazones, diamantes, trebol);
		return this.hayPoker(cantidades);
	}

	private boolean esColor() {
		List<Integer> cantidades = new ArrayList<Integer>();
		this.operacionSobrePalos();
		cantidades = this.cantidades(pica, corazones, diamantes, trebol);
		return this.hayColor(cantidades);
	}
	
	private void operacionSobrePalos() {
		for(Carta carta : this.getCartas()) {
			if (this.esPica(carta)) {
				this.setPica(this.getPica() + 1);
			}
			if (this.esCorazon(carta)) {
				this.setCorazones(this.getCorazones() + 1);
			}
			if (this.esDiamante(carta)) {
				this.setDiamantes(this.getDiamantes() +1 );
			}
			if (this.esTrebol(carta)) {
				this.setTrebol(this.getTrebol() + 1);
			}
		}
	}
	
	private boolean esTrio() {
		Integer contador = 0;
		List<Integer> cantidades = new ArrayList<Integer>();
		for(Integer valor: this.numerosCartas()) {
			for(Carta carta : this.getCartas()) {
				if(valor == carta.getValor()) {
					contador += 1;
				}
				else contador += 0;
			}
			cantidades.add(contador);
			contador = 0;
		}
		return this.hayTrio(cantidades);
	}
	
	private Set<Integer> numerosCartas(){
		Set<Integer>conjunto = new HashSet<Integer>();
		for (Carta carta: this.getCartas()) {
			conjunto.add(carta.getValor());
		}
		return conjunto;
	}
	
	
	private boolean hayTrio(List<Integer> cantidades) {
		return cantidades.contains(3);
	}

	private List<Integer> cantidades(int pica, int corazones, int diamantes, int trebol){
		List<Integer> cantidades = new ArrayList<Integer>();
		cantidades.add(pica);
		cantidades.add(corazones);
		cantidades.add(diamantes);
		cantidades.add(trebol);
		return cantidades;
	}
	
}

