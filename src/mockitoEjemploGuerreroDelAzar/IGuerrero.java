package mockitoEjemploGuerreroDelAzar;

public interface IGuerrero {
	public void atacar(IGuerrero unGuerrero);
	public void descontarVida(Integer unosPuntosDeVida);
}
