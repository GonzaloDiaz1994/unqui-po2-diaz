package mockitoEjemploGuerreroDelAzar;

public interface IMoneda {
	public void sortear();
	public Boolean esCara();
	public Boolean esCruz();
}
