package ar.edu.unq.po2.historiaClinicaSimplificada;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.BeforeEach;

class HistoriaClinicaSimplificadaTest {
	
	HistoriaClinica paciente1;
	AtencionDeGuardia atencion1;
	AtencionDeInternacion atencion2;
	AtencionDeInternacion atencion3;
	Opinion opinion1;
	Opinion opinion2;
	Opinion opinion3;
	Medico medico1;
	Medico medico2;


	@BeforeEach
	void setUp() throws Exception {
		
		medico1 = new Medico(1, "Gonzalo", "Diaz");
		medico2 = new Medico(2, "Pablo", "Diaz");
				
		opinion1 = new Opinion("Me atendieron muy bien", 8);
		opinion2 = new Opinion(null, 9);
		opinion3 = new Opinion(null, 7);
		
		paciente1 = new HistoriaClinica("Fernando", "Gutierrez", "10/10/1969");
		
		atencion1 = new AtencionDeGuardia("Paciente se presenta a la guardia con tos y fiebre", 
				38.5d, true, false, opinion1);
		atencion2 = new AtencionDeInternacion("Paciente con evolucion favorable. Afebril, respira por sus propios medios",
				37d, false, false, opinion2, false);
		atencion3 = new AtencionDeInternacion("Paciente con evolucion desfavorable. Se le indica oxigeno",
				37d, false, true, opinion3, true);	
		
		paciente1.agregarAtencion(atencion1);
		paciente1.agregarAtencion(atencion2);
		paciente1.agregarAtencion(atencion3);
		
		atencion1.agregarMedico(medico1);
		atencion1.agregarMedico(medico2);
		atencion2.agregarMedico(medico1);
		atencion3.agregarMedico(medico1);
	}

	@Test
	void testCantidadEvolucionesDeRiesgoEs2() {
		assertEquals(2, paciente1.cantidadDeEvolucionesDeRiesgo());
	}
	
	@Test
	void testPromedioDeRankigEs8() {
		assertEquals(8, paciente1.promedio());
	}
	
	@Test
	void testPromedioDeOpiniones() {
		assertEquals(8, medico2.promediosOpiniones());
		assertEquals(8, medico1.promediosOpiniones());
	}

}
