package ar.edu.unq.po2.restaurantDeEnsaladas;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RestauranteDeEnsaladasTest {

	PlatoTradicional tomateHuevo;
	PlatoTradicional zanahoriaHuevo;
	PlatoGourmet ananaPalmitos;
	
	Chef huertas;
	
	Opinion opinion1;
	Opinion opinion2;
	Opinion opinion3;
	
	Carta carta;
	
	@BeforeEach
	void setUp() {
		carta = new Carta("Ensaladas");
		
		opinion1 = new Opinion("Bastante buena", 7);
		opinion2 = new Opinion("Anana pasada", 2);
		opinion3 = new Opinion("Muy muy rica ensalada. La recomiendo", 9);
		
		huertas = new Chef("Julian Huertas", 4);
		
		tomateHuevo = new PlatoTradicional("tomate con huevo", 1,100d);
		zanahoriaHuevo = new PlatoTradicional("zanahoria, huevo, rucula, parmesano",
				4, 100d);
		ananaPalmitos = new PlatoGourmet("anana, palmitos, queso de cabra y nueces",
				200d, huertas);
		ananaPalmitos.agregarOpinion(opinion1);
		ananaPalmitos.agregarOpinion(opinion2);
		ananaPalmitos.agregarOpinion(opinion3);
		
		carta.agregarPlatos(ananaPalmitos);
		carta.agregarPlatos(tomateHuevo);
		carta.agregarPlatos(zanahoriaHuevo);
		
	}

	@Test
	void testNombresPlatos() {
		assertEquals("tomate con huevo", tomateHuevo.getNombre());
		assertEquals("zanahoria, huevo, rucula, parmesano", zanahoriaHuevo.getNombre());
		assertEquals("anana, palmitos, queso de cabra y nueces", ananaPalmitos.getNombre());
	}

	@Test
	void testPreciosBases() {
		assertEquals(100d, tomateHuevo.getPrecioBase());
		assertEquals(100d, zanahoriaHuevo.getPrecioBase());
		assertEquals(200d, ananaPalmitos.getPrecioBase());
	}
	
	@Test
	void testChef() {
		assertEquals("Julian Huertas", ananaPalmitos.getChef().getNombre());
		assertEquals(4, ananaPalmitos.getChef().getRanking());
	}
	
	@Test
	void testPreciosFinales() {
		assertEquals(120, tomateHuevo.precioFinal());
		assertEquals(140, zanahoriaHuevo.precioFinal());
		assertEquals(600, ananaPalmitos.precioFinal());
	}
	
	@Test
	void testPromedio() {
		assertEquals(6,ananaPalmitos.promedio());
	}
	
	@Test
	void testPrecioTotalDeLosPlatosActivos() {
		assertEquals(860, carta.sumaPlatosActivos());
	}
	
	@Test
	void testPrecioTotalDeLosPlatosDesactivandoTomateHuevo() {
		tomateHuevo.setActivoParaLaCompra(false);
		assertEquals(740, carta.sumaPlatosActivos());
	}
	
	
}





