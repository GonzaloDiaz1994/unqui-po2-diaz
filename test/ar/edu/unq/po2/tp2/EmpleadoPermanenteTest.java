package ar.edu.unq.po2.tp2;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmpleadoPermanenteTest {	

	private EmpleadoPermanente empleado1;
	private EmpleadoPermanente empleado3;
	
	@BeforeEach
	void setUp() throws Exception {
		
		empleado1 = new EmpleadoPermanente("gonzalo", "calle 554", "soltero", "1994-03-16", 2000.00, 0, 5, false);
		empleado3 = mock(EmpleadoPermanente.class);
		when(empleado3.getTieneConyugue()).thenReturn(true);
	}

	@Test
	void testNombreEmpleado() {
		assertEquals("gonzalo", empleado1.getNombre());
	}
	
	@Test
	public void testDireccion() {
		assertEquals("calle 554", empleado1.getDireccion());
	}
	
	@Test
	public void testEstadoCivil() {
		assertEquals("soltero", empleado1.getEstadoCivil());
	}
	
	@Test
	public void testFechaDeNacimiento() {
		assertEquals("1994-03-16", empleado1.getFechaDeNacimiento());
	}
	
	@Test
	public void testEdad() {
		assertEquals(26, empleado1.edad());
	}
	
	@Test
	public void testSueldoBasico() {
		assertEquals(2000.00, empleado1.getSueldoBasico());
	}
	
	@Test
	public void testCAntidadDeHijos() {
		assertEquals(0, empleado1.getCantidadDeHijos());
	}
	
	@Test
	public void testAniosAntiguedad() {
		assertEquals(5, empleado1.getAniosAntiguedad());
	}
	
	@Test
	public void noTieneConyugue() {
		assertFalse(empleado1.getTieneConyugue());
	}
	
	@Test
	public void testTieneConyugue() {
		assertTrue(empleado3.getTieneConyugue());
	}
	
	@Test
	public void testAsignacionPorHijo() {
		assertEquals(0, empleado1.asignacionPorHijo());
	}
	
	@Test
	public void testAsignacionPorConyugue() {
		assertEquals(0, empleado1.asignacionPorConyugue());
	}
	
	@Test
	public void testExtraPorAntiguedad() {
		assertEquals(250, empleado1.extraPorAntiguedad());
	}
	
	@Test
	public void testSalarioFamiliar() {
		assertEquals(0, empleado1.salarioFamiliar());
	}
	
	@Test
	public void testSueldoBruto() {
		assertEquals(2250.00, empleado1.sueldoBruto());
	}
	
	@Test
	public void testObraSocial() {
		assertEquals(225, empleado1.obraSocial());
	}
	
	@Test
	public void testAportesJubilatorios() {
		assertEquals(337.50, empleado1.aportesJubilatorios());
	}
	
	@Test
	public void testRetenciones() {
		assertEquals(562.50, empleado1.retenciones());
	}
	
	@Test
	public void testSueldoNeto() {
		assertEquals(1687.50, empleado1.sueldoNeto());
	}
}





















