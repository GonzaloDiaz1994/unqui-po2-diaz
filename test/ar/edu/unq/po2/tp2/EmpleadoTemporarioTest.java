package ar.edu.unq.po2.tp2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmpleadoTemporarioTest {

	private EmpleadoTemporario empleado1;

	@BeforeEach
	void setUp() throws Exception {
		
		empleado1 = new EmpleadoTemporario("gonzalo", "calle 554", "casado", "1994-03-16", 2000.00, "2020-12-12", 10);
	}

	@Test
	void testNombreEmpleado() {
		assertEquals("gonzalo", empleado1.getNombre());
	}
	
	@Test
	public void testDireccion() {
		assertEquals("calle 554", empleado1.getDireccion());
	}
	
	@Test
	public void testEstadoCivil() {
		assertEquals("casado", empleado1.getEstadoCivil());
	}
	
	@Test
	public void testFechaDeNacimiento() {
		assertEquals("1994-03-16", empleado1.getFechaDeNacimiento());
	}
	
	@Test
	public void testEdad() {
		assertEquals(26, empleado1.edad());
	}
	
	@Test
	public void testSueldoBasico() {
		assertEquals(2000.00, empleado1.getSueldoBasico());
	}
	
	@Test
	public void testFechaFinDeContrato() {
		assertEquals("2020-12-12", empleado1.getFechaFinDeContrtato());
	}
	
	@Test
	public void testHorasExtras() {
		assertEquals(10, empleado1.getCantidadHorasExtras());
	}
	
	@Test
	public void testSueldoBruto() {
		assertEquals(2400, empleado1.sueldoBruto());
	}
	
	@Test
	public void testRetenciones() {
		assertEquals(530, empleado1.retenciones());
	}
	
	
	@Test
	public void testSueldoNeto() {
		assertEquals(1870.00, empleado1.sueldoNeto());
	}
}




















