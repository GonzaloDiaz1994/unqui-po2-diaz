package ar.edu.unq.po2.tp2;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class testEmpleadoPermanente {

	private EmpleadoPermanente empleado1;
	private EmpleadoTemporario empleado2;
	private Empresa sancor;
	private ReciboDeHaberes recibo1;
	private ReciboDeHaberes recibo2;
	
	
	@BeforeEach
	void setUp() {
		
		empleado1 = mock(EmpleadoPermanente.class);
		empleado2 = mock(EmpleadoTemporario.class);
		when(empleado1.sueldoNeto()).thenReturn(5000.00);
		when(empleado2.sueldoNeto()).thenReturn(2000.00);
		
		when(empleado1.sueldoBruto()).thenReturn(4000.00);
		when(empleado2.sueldoBruto()).thenReturn(1000.00);
		
		when(empleado1.retenciones()).thenReturn(1000.00);
		when(empleado2.retenciones()).thenReturn(1000.00);
		
		recibo1 = mock(ReciboDeHaberes.class);
		recibo2 = mock(ReciboDeHaberes.class);		
	
		sancor = new Empresa("Sancor", 123);
		sancor.agregarEmpleado(empleado1);
		sancor.agregarEmpleado(empleado2);
		sancor.agregarRecibo(recibo1);
		sancor.agregarRecibo(recibo2);
	}
	
	@Test
	public void testNombreEmpresa() {
		assertEquals("Sancor", sancor.getNombre());
	}
	
	@Test
	public void testCuitEmpresa() {
		assertEquals(123, sancor.getCuit());
	}
	
	@Test
	public void testEmpresaEmpleados() {
		assertEquals(2, sancor.getEmpleados().size());
	}
	
	@Test
	public void testRecibosSon2() {
		assertEquals(2, sancor.getReciboDeHaberes().size());
	}
	
	@Test
	public void testEliminarEmpleado() {
		sancor.eliminarEmpleado(empleado1);
		assertEquals(1, sancor.getEmpleados().size());
	}	
	
	@Test
	public void testMmontoTotalNeto() {
		assertEquals(7000, sancor.montoTotalSueldosNetos());
	}
	
	@Test
	public void testMontoTotalBruto() {
		assertEquals(5000.00, sancor.montoTotalSueldosBrutos());
	}
	
	@Test
	public void testMontoTotalRetenciones() {
		assertEquals(2000.00, sancor.montoTotalRetenciones());
	}
	
	@Test
	public void testLiquidacionDeSueldos() {
		sancor.lisquidacionDeSueldos();
		assertEquals(4, sancor.getReciboDeHaberes().size());
	}
	
}
