package ar.edu.unq.po2.tp2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReciboDeHaberesTest {
	
	private ReciboDeHaberes recibo1;


	@BeforeEach
	void setUp() throws Exception {
		recibo1 = new ReciboDeHaberes("gonzalo", "calle 554", 2500.00, 2200.00, "sueldo");
	}

	@Test
	void testNombreEmpleado() {
		assertEquals("gonzalo", recibo1.getNombreEmpleado());
	}
	
	@Test
	public void testDireccionEmpleado() {
		assertEquals("calle 554", recibo1.getDireccionEmpleado());
	}
	
	@Test
	public void testSueldoBrutoEmpleado() {
		assertEquals(2500, recibo1.getSueldoBruto());
	}
	
	@Test
	public void testSueldoNetoEmpleado() {
		assertEquals(2200, recibo1.getSueldoNeto());
	}
	
	@Test
	public void testConcepto() {
		assertEquals("sueldo", recibo1.getDesgloceDeConceptos());
	}
	
	@Test
	public void testFechaDeEmisionDeRecibo() {
		LocalDate hoy = LocalDate.now();
		assertEquals(hoy, recibo1.getFechaDeEmisionDelRecibo());
	}

}
