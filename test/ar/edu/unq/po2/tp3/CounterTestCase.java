package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CounterTestCase {

	Counter counter;
	
	@BeforeEach
	public void setUp() {
		
		counter = new Counter();
		
		counter.agregarNumero(1);
		counter.agregarNumero(3);
		counter.agregarNumero(5);
		counter.agregarNumero(7);
		counter.agregarNumero(9);
		counter.agregarNumero(1);
		counter.agregarNumero(1);
		counter.agregarNumero(1);
		counter.agregarNumero(1);
		counter.agregarNumero(4);
		
	}
		
	@Test
	void testNumerosImpares() {
		assertEquals(9, counter.cantidadImpares());
	}
	
	@Test
	void testNumerosPares() {
		assertEquals(1, counter.cantidadPares());
	}

	@Test
	void testNumerosMultiplosDe3() {
		assertEquals(2, counter.cantidadMultiplosDe(3));
	}
}
