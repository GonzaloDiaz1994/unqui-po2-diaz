package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EquipoDeTrabajoTest {

	EquipoDeTrabajo equipo;
	Persona gonzalo;
	Persona pablo;
	Persona carlos;
	Persona mirta;
	Persona tyzon;
	
	@BeforeEach
	void setUp(){
		
		equipo = new EquipoDeTrabajo("Royal");
		gonzalo = new Persona ("gonzalo", "1994-03-16");
		pablo = new Persona ("pablo", "1997-08-01");
		carlos = new Persona("carlos", "1970-02-05");
		mirta = new Persona("mirta", "1970-10-13");
		tyzon = new Persona("tyzon", "2010-09-24");
		
		equipo.agregarIntegrante(carlos);
		equipo.agregarIntegrante(gonzalo);
		equipo.agregarIntegrante(mirta);
		equipo.agregarIntegrante(pablo);
		equipo.agregarIntegrante(tyzon);
	}

	@Test
	void testEdadGonzalo() {
		assertEquals(26, gonzalo.edad());
	}
	
	@Test
	void testPromedioDeEdades() {
		String valor =  equipo.promedioDeEdad().toString();
		assertEquals(31.2, equipo.promedioDeEdad());
		System.out.println("Este es el promedio de edad de los integrantes: " + valor);
	}

}
