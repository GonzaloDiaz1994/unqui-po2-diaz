package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MultioperadorTest {

	Multioperador multioperador;

	@BeforeEach
	void setUp() throws Exception {
		multioperador = new Multioperador();
		multioperador.agregarNumero(1);
		multioperador.agregarNumero(2);
		multioperador.agregarNumero(3);
		multioperador.agregarNumero(1);
		multioperador.agregarNumero(2);
		
	}

	@Test
	void testSumarTodo() {
	assertEquals(9, multioperador.sumarNumeros());
	}

	@Test
	void testRestarTodo() {
		assertEquals(-7, multioperador.restarNumeros());
	}
	
	@Test
	void testMultiplicarTodo() {
		assertEquals(12, multioperador.multiplicarNumeros());
	}
}

