package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonaTest {

	Persona gonzalo;
	Persona pablo;
	
	@BeforeEach
	void setUp(){
		
		gonzalo = new Persona("Gonzalo", "1994-03-16");
		pablo = new Persona("Pablo", "1997-08-01");
	}

	@Test
	void testEdad() {
		assertEquals(26, gonzalo.edad());
		assertEquals(22, pablo.edad());
	}

	@Test
	void pabloEsMenorQueGonzalo() {
		assert(pablo.menorQue(gonzalo));
	}
}
