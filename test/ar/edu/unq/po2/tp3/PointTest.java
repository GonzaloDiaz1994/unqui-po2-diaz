package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PointTest {

	Point punto;
	Point otroPunto;
	
	@BeforeEach
	void setUp() throws Exception {
		punto = new Point(2,3);
		otroPunto = new Point(4,1);
		
	}

	@Test
	void testMoverPunto() {
		punto.moverPunto(4, 5);
		assertEquals(4, punto.getX());
		assertEquals(5, punto.getY());
	}

	@Test
	void testSumarPuntos() {
		Point resultado = punto.sumarConOtroPunto(otroPunto);
		assertEquals(6, resultado.getX());
		assertEquals(4, resultado.getY());
	}
	
}
