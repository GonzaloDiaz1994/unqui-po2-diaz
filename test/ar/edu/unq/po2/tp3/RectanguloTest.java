package ar.edu.unq.po2.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RectanguloTest {

	private Rectangulo rectangulo;
	private Point punto1;
	private Point punto2;
	
	@BeforeEach
	void setUp(){
		
		punto1 = new Point(3, 2);
		punto2 = new Point(5, 5);
		rectangulo = new Rectangulo(punto1, punto2);
	}

	@Test
	void testArea() {
		assertEquals(6, rectangulo.calcularArea());
	}

	@Test
	void testPerimetro() {
		assertEquals(10, rectangulo.calcularPerimetro());
	}
	
	@Test
	void testEsHorizontalTieneQueDarFalse() {
		assertFalse(rectangulo.esHorizontal());
	}
	
	@Test
	void testEsVerticalTieneQueSerTrue() {
		assertTrue(rectangulo.esVertical());
	}
}
