package ar.edu.unq.po2.tp4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TrabajadorTest {

	private Trabajador trabajador;
	private Ingreso ingreso1;
	private Ingreso ingreso2;
	private IngresoPorHorasExtras ingresoHorasExtras1;
	private IngresoPorHorasExtras ingresoHorasExtras2;
	
	
	@BeforeEach
	public void setUp() throws Exception {
		trabajador = new Trabajador();
		ingreso1 = new Ingreso("enero", "sueldo", 10d);
		ingreso2 = new Ingreso("febrero", "sueldo", 12d);
		ingresoHorasExtras1 = new IngresoPorHorasExtras("enero", "sueldo", 5d, 2);
		ingresoHorasExtras2 = new IngresoPorHorasExtras("febrero", "sueldo", 7d, 3);
		
		trabajador.agregarIngreso(ingreso1);
		trabajador.agregarIngreso(ingreso2);
		trabajador.agregarIngreso(ingresoHorasExtras1);
		trabajador.agregarIngreso(ingresoHorasExtras2);
	}

	@Test
	public void testTotalPercibido() {
		assertEquals(34, trabajador.getTotalPercibido());
	}
	
	@Test
	public void testTotalImponible() {
		assertEquals(22, trabajador.getMontoImponible());
	}
	
	@Test
	public void testImpuestoAPagar() {
		assertEquals(0.44, trabajador.getImpuestoAPagar());
	}

}
