package ar.edu.unq.po2.tp5;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CajaTest {

	private Caja caja1;
	private ProductoTradicional arroz;
	private ProductoTradicional gaseosa;
	private ProductoDeCooperativa leche;
	private ProductoDeCooperativa agua;

	private Servicio gas;
	private Servicio electricidad;
	
	private Impuesto automotor;
	private Impuesto alumbrado;
	
	private Agencia agencia;
	
	@BeforeEach
	public void setUp() throws Exception {
	
		arroz = new ProductoTradicional(20d, 5);
		gaseosa = new ProductoTradicional(120d, 10);
		leche = new ProductoDeCooperativa(10d, 5);
		agua = new ProductoDeCooperativa(10d, 50);
		
		gas = new Servicio(3, 250);
		electricidad = new Servicio(2, 500);
		
		automotor = new Impuesto(500);
		alumbrado = new Impuesto(200);
		
		caja1 = new Caja();
		
		caja1.registrarProducto(arroz);
		caja1.registrarProducto(gaseosa);
		caja1.registrarProducto(leche);
		caja1.registrarProducto(agua);
		
		agencia = mock(Agencia.class);
	}

	@Test
	public void montoProductosDeCooperativaTest() {
		assertEquals(9d, leche.getMontoAPagar());
		assertEquals(4, leche.getStock());
		assertEquals(9, agua.getMontoAPagar());
		assertEquals(49, agua.getStock());
	}
	
	@Test
	public void totalAPagarTest() {
		assertEquals(158d, caja1.getTotalAPagar());
	}
	
	@Test
	public void notificacionALaAgencia() {
		caja1.cobrarFactura(agencia, gas);
		caja1.cobrarFactura(agencia, electricidad);
		caja1.cobrarFactura(agencia, automotor);
		caja1.cobrarFactura(agencia, alumbrado);
		verify(agencia, times(1)).registrarPago(gas);
		verify(agencia, times(1)).registrarPago(electricidad);
		verify(agencia, times(1)).registrarPago(automotor);
		verify(agencia, times(1)).registrarPago(alumbrado);
	}
	
	@Test
	public void cobrarFacturas() {
		caja1.cobrar();
		caja1.cobrarFactura(agencia, gas);
		caja1.cobrarFactura(agencia, electricidad);
		caja1.cobrarFactura(agencia, automotor);
		caja1.cobrarFactura(agencia, alumbrado);
		assertEquals(2450, caja1.getTotalAPagar());
	}
	
}
