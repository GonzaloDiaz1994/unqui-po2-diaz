package ar.edu.unq.po2.tp6.bancoYPrestamos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BancoYPrestamoTest {
	
	private Banco frances;
	private Cliente gonzalo;
	private Cliente pablo;
	private Cliente tyzon;
	private Propiedad casa;
	private Propiedad bar;
	private Propiedad departamento;
	private SolicitudDeCredito solicitud1;
	private SolicitudDeCredito solicitud2;
	private SolicitudDeCredito solicitud3;
	
	@BeforeEach
	void setUp() throws Exception {
		frances = new Banco();
		
		casa = new Propiedad("es linda", "calle falsa 123", 50000);
		bar = new Propiedad("esta limpio", "san jorge", 100000);
		departamento = new Propiedad("es gigante", "varela", 400000);
		
		gonzalo = new Cliente("gonzalo", "diaz", "calle falsa 123", 26, 12000, casa);
		pablo = new Cliente("pablo", "diaz", "san jorge", 22, 500, bar);
		tyzon = new Cliente("tyzon", "diaz", "varela", 9, 50000, departamento);
		
		solicitud1 = gonzalo.solicitarCreditoPersonal(10000, 36);
		solicitud2 = pablo.solicitarCreditoPersonal(50000, 72);
		solicitud3 = tyzon.solicitarCreditoHipotecario(1000000, 144);
		
		frances.agregarCliente(gonzalo);
		frances.agregarCliente(pablo);
		frances.agregarCliente(tyzon);
		
		frances.agregarSolicitudDeCredito(solicitud1);
		frances.agregarSolicitudDeCredito(solicitud2);
		frances.agregarSolicitudDeCredito(solicitud3);
		
	}
	@Test
	public void cuotaMensual() {
		assertEquals(277d, solicitud1.montoCuotaMensual());
	}
	
	@Test
	public void sueldoMensual() {
		assertEquals(12000, gonzalo.getSueldoMensual());
	}
	
	@Test
	public void valorConPorcentaje() {
		double total = 70 / 100d;
		assertEquals(0.7, total);
	}
	
	@Test
	public void cuotaNoSupera70() {
		assertTrue(solicitud1.cuotaNoSuperaPorcentaje(70));
	}
	
	@Test
	void solicitudEsAceptable() {
		assertTrue(solicitud1.esAceptable());
		assertFalse(solicitud2.esAceptable());
		assertFalse(solicitud3.esAceptable());
	}
	
	@Test
	public void otorgarCreditos() {
		assertEquals(3, frances.getSolicitudes().size());
		frances.evaluarSolicitudes();
		assertEquals(10000, gonzalo.getEfectivo());
		assertEquals(0, pablo.getEfectivo());
		assertEquals(0, tyzon.getEfectivo());
		assertEquals(0, frances.getSolicitudes().size());
	}
	
	@Test
	public void montoADesembolsar() {
		assertEquals(10000, frances.montoTotalADesembolsar());
	}


}
