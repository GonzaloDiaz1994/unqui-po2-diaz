package ar.edu.unq.po2.tp6.clienteEmail;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ClienteEmailTest {

	private ClienteEMail cliente1;
	private ServidorPop servidorPop;
	
	private ClienteEMail cliente2;
	
	private Correo correo1;
	private Correo correo2;
	
	
	@BeforeEach
	void setUp() throws Exception {
		
		servidorPop = mock(ServidorPop.class);
		servidorPop.agregarUsuario(cliente1);
		servidorPop.agregarUsuario(cliente2);
		
		correo1 = mock(Correo.class);
		when(correo1.getDestinatario()).thenReturn("pablo");
//		correo2 = new Correo(null, "pablo", null);
		
		cliente1 = new ClienteEMail(servidorPop, "gonzalo", "1234");
		cliente1.agregarCorreo(correo1);
		cliente1.agregarCorreo(correo2);
		
		cliente2 = new ClienteEMail(servidorPop, "pablo", "5678");
		
		cliente2 = mock(ClienteEMail.class);
		when(cliente2.getServidor()).thenReturn(servidorPop);
		when(cliente2.getNombreUsuario()).thenReturn("pablo");
		
		
		
	}

	@Test
	public void testGetters() {
		assertEquals("gonzalo", cliente1.getNombreUsuario());
		assertEquals("1234", cliente1.getPassUsuario());
	}

	@Test
	public void testNoEstaConectado() {
		assertFalse(cliente1.getEstaConectado());
	}
	
	@Test
	public void testEstaConectado() {
		cliente1.iniciarSesion();
		assertTrue(cliente1.getEstaConectado());
	}
	
	@Test
	public void testConectar() {
		cliente1.conectar();
		verify(servidorPop, times(1)).conectar(cliente1.getNombreUsuario(), cliente1.getPassUsuario());
	}
	
	@Test
	public void testCorreos() {
		assertEquals(2, cliente1.contarInbox());
		assertEquals(0, cliente1.contarBorrados());
	}
	
	@Test
	public void testBorradosDa1() {
		cliente1.borrarCorreo(correo1);
		assertEquals(1, cliente1.contarInbox());
		assertEquals(1, cliente1.contarBorrados());
	}

	@Test
	public void testEliminarBorrados() {
		cliente1.borrarCorreo(correo1);
		assertEquals(1,cliente1.contarBorrados());
		cliente1.eliminarBorrado(correo1);
		assertEquals(0, cliente1.contarBorrados());
		
	}
/*	
	@Test
	public void testEnviarCorreoACliente2() {
		cliente1.enviarCorreo(correo2);
		assertEquals(1, cliente2.contarInbox());
	}
*/
}





















