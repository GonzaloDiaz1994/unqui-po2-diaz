package ar.edu.unq.po2.tp6.clienteEmail;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CorreoTest {

	private Correo correo1;

	@BeforeEach
	public void setUp() throws Exception {
		
		correo1 = new Correo("ninguno", "gonzalo", "hola");
	}

	@Test
	public void testAsunto() {
		assertEquals("ninguno", correo1.getAsunto());
	}
	
	@Test
	public void testDestinatario() {
		assertEquals("gonzalo", correo1.getDestinatario());
	}
	
	@Test
	public void testCuerpo() {
		assertEquals("hola", correo1.getCuerpo());
	}

}
