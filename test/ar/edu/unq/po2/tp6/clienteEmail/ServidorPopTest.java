package ar.edu.unq.po2.tp6.clienteEmail;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ServidorPopTest {
	
	private ServidorPop servidorPop;
	private ClienteEMail cliente1;
	private ClienteEMail cliente2;
	
	private Correo correo1;
	
	@BeforeEach
	public void setUp() throws Exception {
		
		cliente1 = mock(ClienteEMail.class);
		cliente2 = mock(ClienteEMail.class);
		
		correo1 = mock(Correo.class);
		
		servidorPop = new ServidorPop();
		
		servidorPop.agregarUsuario(cliente1);
		servidorPop.agregarUsuario(cliente2);
		
		when(cliente1.getNombreUsuario()).thenReturn("gonzalo");
		when(cliente1.getPassUsuario()).thenReturn("1234");
		
		when(cliente2.getNombreUsuario()).thenReturn("pablo");
		when(cliente2.getPassUsuario()).thenReturn("5678");
		
		when(correo1.getDestinatario()).thenReturn("pablo");
	}

	@Test
	public void testCantidadClientes() {
		assertEquals(2, servidorPop.getBaseDeDatos().size());
	}
	
	@Test
	public void conectarCliente1() {
		servidorPop.conectar("gonzalo", "1234");
		verify(cliente1, times(1)).iniciarSesion();
	}
	
	@Test
	public void enviarCorreoTest() {
		servidorPop.enviar(correo1);
		verify(cliente2, times(1)).agregarCorreo(correo1);;
	}

	@Test
	public void recibirNuevosTest() {
		assertEquals(0, servidorPop.recibirNuevos(cliente1).size());
		verify(cliente1, times(1)).getInbox();
	}
}










