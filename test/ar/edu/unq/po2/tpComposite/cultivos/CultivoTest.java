package ar.edu.unq.po2.tpComposite.cultivos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CultivoTest {

	private Soja soja;
	private Trigo trigo;
	private Mixto mixto1;
	private Mixto mixto2;
	
	@BeforeEach
	void setUp() throws Exception {
		soja = new Soja();
		trigo = new Trigo();
		mixto1 = new Mixto();
		mixto2 = new Mixto();
		
		mixto1.agregarCultivo(soja);
		mixto1.agregarCultivo(soja);
		mixto1.agregarCultivo(trigo);
		mixto1.agregarCultivo(trigo); //400
		
		mixto2.agregarCultivo(soja);
		mixto2.agregarCultivo(trigo);
		mixto2.agregarCultivo(mixto1);//100
		mixto2.agregarCultivo(soja); //325
	}

	@Test
	public void testPrecioSoja() {
		assertEquals(500, soja.getPrecio());
	}
	@Test
	public void testPrecioTrigo() {
		assertEquals(300, trigo.getPrecio());
	}
	
	@Test
	public void testPrecioMixto1() {
		assertEquals(400, mixto1.getPrecio());
	}
	
	@Test
	public void testPrecioMixto2() {
		assertEquals(425, mixto2.getPrecio());
	}
}
