package ar.edu.unq.po2.tpComposite.shapeShifter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ShapeShifterTest {
	
	private IShapeShifter unaHoja;
	private IShapeShifter otraHoja;
	private IShapeShifter tercerHoja;
	
	@BeforeEach
	void setUp() throws Exception {
		unaHoja = new Contenido(1);
		otraHoja = new Contenido(2);
		tercerHoja = new Contenido(3);
	}

	@Test
	void testUnaHojaCompuestaConOtraHojaMeDaCompuestoDeLasDosHojas() {
		Contenido unaHoja = new Contenido(1);
		Contenido otraHoja = new Contenido(2);
		assertEquals(unaHoja.compose(otraHoja).getContenidos().size(), 2);
		assertTrue(unaHoja.compose(otraHoja).getContenidos().contains(unaHoja));
		assertTrue(unaHoja.compose(otraHoja).getContenidos().contains(otraHoja));
	}

	@Test
	void testUnCompuestoCompuestoConUnaHojaMeDaCompuestoDeAmbos() {
		IShapeShifter unCompuesto = unaHoja.compose(otraHoja);
		assertEquals(unCompuesto.compose(unaHoja).getContenidos().size(), 2);
		assertTrue(unCompuesto.compose(unaHoja).getContenidos().contains(unCompuesto));
		assertTrue(unCompuesto.compose(unaHoja).getContenidos().contains(unaHoja));
	}
	
	@Test
	void testUnCompuestoSabeSuProfundidad() {
		IShapeShifter unCompuesto = unaHoja.compose(otraHoja);
		assertEquals(unaHoja.deepest(), 0);
		assertEquals(unCompuesto.deepest(), 1);
		assertEquals(unCompuesto.compose(unCompuesto).deepest(), 2);
	}
	
	@Test
	void testUnCompuestoSabeAplanar() {
		IShapeShifter unCompuesto = unaHoja.compose(otraHoja);
		IShapeShifter otroCompuesto = unCompuesto.compose(tercerHoja);
		assertEquals(otroCompuesto.flat().getContenidos().size(), 3);
		assertFalse(otroCompuesto.flat().getContenidos().contains(unCompuesto));
	}
	
	@Test
	void testUnCompuestoSabeDevolverSusValores() {
		IShapeShifter unCompuesto = unaHoja.compose(otraHoja);
		IShapeShifter otroCompuesto = unCompuesto.compose(tercerHoja);
		assertEquals(otroCompuesto.values().size(), 3);
		assertTrue(otroCompuesto.values().contains(1));
		assertTrue(otroCompuesto.values().contains(2));
		assertTrue(otroCompuesto.values().contains(3));
	}
}