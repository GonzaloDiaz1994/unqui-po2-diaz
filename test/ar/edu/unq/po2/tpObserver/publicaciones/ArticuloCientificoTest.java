package ar.edu.unq.po2.tpObserver.publicaciones;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ArticuloCientificoTest {

	private ArticuloCientifico articulo;

	@BeforeEach
	void setUp() throws Exception {
		List<String> autores = new ArrayList<String>();
		List<String> palabrasClaves = new ArrayList<String>();
		autores.add("Gamma");
		autores.add("Gonzalo");
		palabrasClaves.add("quilmes");
		palabrasClaves.add("Gamma");
		articulo = new ArticuloCientifico("Patrones", autores, "Quilmes", "Investigacion", "Quilmes", palabrasClaves);
	}

	@Test
	public void testAutores() {
		assertEquals(2, articulo.getAutores().size());
	}
	
	@Test
	public void testTitulo() {
		assertEquals("Patrones", articulo.getTitulo());
	}

	@Test
	public void testFiliacion() {
		assertEquals("Quilmes", articulo.getFiliacion());
	}
	
	@Test
	public void testTipoArticulo() {
		assertEquals("Investigacion", articulo.getTipoArticulo());
	}
	
	@Test
	public void tesLugarPublicacion() {
		assertEquals("Quilmes", articulo.getLugarPublicacion());
	}
	
	@Test
	public void testPalabrasClaves() {
		assertEquals(2, articulo.getPalabrasClaves().size());
	}
}
