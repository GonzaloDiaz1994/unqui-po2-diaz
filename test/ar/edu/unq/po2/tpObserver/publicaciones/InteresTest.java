package ar.edu.unq.po2.tpObserver.publicaciones;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InteresTest {

	private Interes interes;
	private ArticuloCientifico articulo;

	@BeforeEach
	void setUp() throws Exception {
	
		articulo = mock(ArticuloCientifico.class);
	}

	@Test
	public void testInteresPorAutor() {
		interes = new InteresPorAutor("gonzalo");
		List<String> listaAutores = new ArrayList<String>();
		listaAutores.add("gonzalo");
		listaAutores.add("pablo");
		when(articulo.getAutores()).thenReturn(listaAutores);
		assertTrue(interes.conincideCon(articulo));
	}

	@Test
	public void testNoHayInteresPorAutor() {
		interes = new InteresPorAutor("gonzalo");
		List<String> listaAutores = new ArrayList<String>();
		listaAutores.add("pablo");
		when(articulo.getAutores()).thenReturn(listaAutores);
		assertFalse(interes.conincideCon(articulo));
	}
	
	@Test
	public void testInteresPorTitulo() {
		interes = new InteresPorTitulo("patrones");
		when(articulo.getTitulo()).thenReturn("patrones");
		assertTrue(interes.conincideCon(articulo));
	}
	
	@Test
	public void testNohayInteresPorTitulo() {
		interes = new InteresPorTitulo("patrones");
		when(articulo.getTitulo()).thenReturn("otroTitulo");
		assertFalse(interes.conincideCon(articulo));
	}
	
	@Test
	public void testInteresPorLugarDePublicacion() {
		interes = new InteresPorLugarDePublicacion("quilmes");
		when(articulo.getLugarPublicacion()).thenReturn("quilmes");
		assertTrue(interes.conincideCon(articulo));
	}
	
	@Test
	public void testNoHayInteresPorLugarDePublicacion() {
		interes = new InteresPorLugarDePublicacion("quilmes");
		when(articulo.getLugarPublicacion()).thenReturn("varela");
		assertFalse(interes.conincideCon(articulo));
	}
	
	@Test
	public void testInteresPorTipoDeArticulo() {
		interes = new InteresPorTipoDeArticulo("investigacion");
		when(articulo.getTipoArticulo()).thenReturn("investigacion");
		assertTrue(interes.conincideCon(articulo));
	}
	
	@Test
	public void testNoHayInteresPorTipoDeArticulo() {
		interes = new InteresPorTipoDeArticulo("investigacion");
		when(articulo.getTipoArticulo()).thenReturn("ciencia");
		assertFalse(interes.conincideCon(articulo));
	}
	
	@Test
	public void testInteresPorFiliacion() {
		interes = new InteresPorFiliacion("varela");
		when(articulo.getFiliacion()).thenReturn("varela");
		assertTrue(interes.conincideCon(articulo));
	}
	
	@Test
	public void testNoHayInteresPorFiliacion() {
		interes = new InteresPorFiliacion("varela");
		when(articulo.getFiliacion()).thenReturn("bosques");
		assertFalse(interes.conincideCon(articulo));
	}
	
	@Test
	public void testInteresPalabraClave() {
		interes = new InteresPorPalabraClave("gonzalo");
		List<String> listaPalabras = new ArrayList<String>();
		listaPalabras.add("gonzalo");
		listaPalabras.add("pablo");
		when(articulo.getPalabrasClaves()).thenReturn(listaPalabras);
		assertTrue(interes.conincideCon(articulo));
	}
	
	@Test
	public void testNoHayInteresPalabraClave() {
		interes = new InteresPorPalabraClave("gonzalo");
		List<String> listaPalabras = new ArrayList<String>();
		listaPalabras.add("pablo");
		when(articulo.getPalabrasClaves()).thenReturn(listaPalabras);
		assertFalse(interes.conincideCon(articulo));
	}
}

















