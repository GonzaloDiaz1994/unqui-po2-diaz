package ar.edu.unq.po2.tpObserver.publicaciones;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InvestigadorTest {

	private Investigador investigador;	
	private ArticuloCientifico articulo;
	private InteresPorTitulo patrones;
	private InteresPorAutor gamma;
	private InteresPorTipoDeArticulo investigacion;
	
	@BeforeEach
	void setUp() throws Exception {
		
		patrones = mock(InteresPorTitulo.class);
		gamma = mock(InteresPorAutor.class);
		investigacion = mock(InteresPorTipoDeArticulo.class);
		
		investigador = new Investigador();
		investigador.addInteres(patrones);
		investigador.addInteres(gamma);
		investigador.addInteres(investigacion);
		
		articulo = mock(ArticuloCientifico.class);
		
	}

	@Test
	public void testTieneComoPreferenciaAlTituloPatrones() {
		assertTrue(investigador.getIntereses().contains(patrones));
	}
	
	@Test
	public void testTieneComoPreferenciaAlAutorGamma() {
		assertTrue(investigador.getIntereses().contains(gamma));
	}
	
	@Test
	public void testTieneComoPreferenciaAlTipoDeArticuloInvestigacion() {
		assertTrue(investigador.getIntereses().contains(investigacion));
	}
	
	@Test
	public void testNoEstaInteresadoEnArticulo() {
		when(articulo.getTitulo()).thenReturn("otroTitulo");
		when(patrones.getInteres()).thenReturn("patrones");
		assertFalse(investigador.estaInteresadoEn(articulo));
	}
	
	@Test
	public void testRecibeNotificacion() {
		investigador.recibirNotificacion(articulo);
		assertEquals(1,investigador.getMisArticulos().size());
	}
}





