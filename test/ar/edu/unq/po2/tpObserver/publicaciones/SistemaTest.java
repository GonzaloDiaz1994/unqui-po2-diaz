package ar.edu.unq.po2.tpObserver.publicaciones;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SistemaTest {

	private Sistema sistema;
	private Investigador investigador1;
	private ArticuloCientifico articulo;
	private InteresPorAutor autor;
	private InteresPorFiliacion filiacion;
	
	
	@BeforeEach
	void setUp() throws Exception {
		
		sistema = new Sistema();
		
		investigador1 = mock(Investigador.class);		
		when(investigador1.estaInteresadoEn(articulo)).thenReturn(true);
		investigador1.addInteres(autor);
				
		articulo = mock(ArticuloCientifico.class);
		when(articulo.getTitulo()).thenReturn("gamma");
		
		autor = mock(InteresPorAutor.class);
		when(autor.getInteres()).thenReturn("gamma");
		
		filiacion = mock(InteresPorFiliacion.class);
		when(filiacion.getInteres()).thenReturn("quilmes");

		
		sistema.agregarSuscriptor(investigador1);
		sistema.updatePublicaciones(articulo);
	}

	@Test
	public void testCantidadDePublicaciones() {
		assertEquals(1, sistema.getPublicaciones().size());	
	}
	
	@Test
	public void tesInvestigadorNoRecibeNotificacion() {
		sistema.notificarSuscriptores(articulo);
		verify(investigador1, times(0)).recibirNotificacion(articulo);
	}
}
