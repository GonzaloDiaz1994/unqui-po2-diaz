package ar.edu.unq.po2.tpStateEstrategy.encriptacion;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tpStateStrategy.encriptacion.EncriptadorNaive;
import ar.edu.unq.po2.tpStateStrategy.encriptacion.EstrategiaCambiarOrden;
import ar.edu.unq.po2.tpStateStrategy.encriptacion.EstrategiaLetraPorNumero;
import ar.edu.unq.po2.tpStateStrategy.encriptacion.EstrategiaVocalSiguiente;

@SuppressWarnings("unused")
class EncriptadorNaiveTest {

	private EncriptadorNaive encriptador;
//	private EstrategiaCambiarOrden cambiarOrden;
	private EstrategiaVocalSiguiente vocalSiguiente;
	private EstrategiaLetraPorNumero letraPorNumero;
	
	
	@BeforeEach
	void setUp() throws Exception {
		encriptador = new EncriptadorNaive();
//		cambiarOrden = new EstrategiaCambiarOrden(); el constrcutor de encriptar inicializa con esta estrategia
		vocalSiguiente = new EstrategiaVocalSiguiente();
		letraPorNumero = new EstrategiaLetraPorNumero();
		
				
	}

	@Test
	public void testEncriptarOrdenDePalabras() {
		assertEquals("aloH", encriptador.encriptar("Hola"));
	}
	
	@Test
	public void testDesencriptarOrdenDePalabras() {
		assertEquals("Hola", encriptador.desencriptar("aloH"));
	}
	
	@Test
	public void testEncriptarVocalSiguiente() {
		encriptador.setEstrategiaEncriptado(vocalSiguiente);
		assertEquals("Hule cumu istes", encriptador.encriptar("Hola como estas"));
	}
	
	@Test
	public void testDesencriptarVocalSiguiente() {
		encriptador.setEstrategiaEncriptado(vocalSiguiente);
		assertEquals("Hola como estas", encriptador.desencriptar("Hule cumu istes"));
	}

	@Test
	public void testEncriptarLetraPorNumero() {
		encriptador.setEstrategiaEncriptado(letraPorNumero);
		assertEquals("4,9,5,7,15,", encriptador.encriptar("diego"));
	}
/*	
	@Test
	public void testDesencriptarLetraPorNumero() {
		encriptador.setEstrategiaEncriptado(letraPorNumero);
		assertEquals("diego", encriptador.desencriptar("4,9,5,7,15,"));
	}
*/
}
