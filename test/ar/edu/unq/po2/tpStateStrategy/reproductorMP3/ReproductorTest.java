package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReproductorTest {

	private Reproductor mp3;
	private Song cancion1;
	private Song cancion2;
	private Song cancion3;
	
	@BeforeEach
	void setUp() throws Exception {
		
		mp3 = new Reproductor();
		
		cancion1 = mock(Song.class);
		cancion2 = mock(Song.class);
		cancion3 = mock(Song.class);
		
		when(cancion2.getNombre()).thenReturn("more");
		
		mp3.agregarCancion(cancion1);
		mp3.agregarCancion(cancion2);
		mp3.agregarCancion(cancion3);
	}

	@Test
	public void testCantidadCanciones() {
		assertEquals(3, mp3.getCanciones().size());
	}

	@Test
	public void testNoHayCancionSeleccionada() {
		assertEquals(null, mp3.getCancionSeleccionada());
	}
	
	@Test
	public void testSeleccionarCancion() {
		mp3.seleccionarCancion(cancion2);
		assertEquals(cancion2, mp3.getCancionSeleccionada());
	}
	
	@Test
	public void testEstadoEnStop() {
		assertTrue(mp3.getEstado() instanceof SeleccionadorDeCanciones );
	}
	
	@Test
	public void testPlay() throws Exception {
		verify(cancion2, times(0)).play();
		mp3.seleccionarCancion(cancion2);
		verify(cancion2, times(0)).play();
		mp3.play();
		verify(cancion2, times(1)).play();
		assertTrue(mp3.getEstado().estaReproduciendo());
	}
	
	@Test
	public void testPause() throws Exception {
		Exception excepcion = assertThrows(Exception.class, () -> mp3.pause());
		assertEquals("No hay nada para pausar", excepcion.getMessage());
	
	}
	
	@Test
	public void testStop() {
		mp3.stop();
		verify(cancion2, never()).stop();
	}
	
	@Test
	public void testNoEstaReproduciendoElMP3() {
		assertFalse(mp3.getEstado().estaReproduciendo());
	}
	
	@Test
	public void testPonerPlay2VecesProduceError() throws Exception{
		mp3.seleccionarCancion(cancion2);
		mp3.play();
		Exception excepcion = assertThrows(Exception.class, () -> mp3.play());
		assertEquals("Ya est� reproduciendo", excepcion.getMessage());
	}
	
	@Test
	public void testPonerStopMientrasReproduce() throws Exception {
		mp3.seleccionarCancion(cancion2);
		mp3.play();
		verify(cancion2, times(1)).play();
		mp3.stop();
		verify(cancion2, times(1)).stop();
	}
/*	
	@Test
	public void testPonerPausaLaReproduccion() throws Exception {
		mp3.seleccionarCancion(cancion2);
		mp3.pause();
		//verify(cancion2, times(1)).pause();
	}
*/

}









