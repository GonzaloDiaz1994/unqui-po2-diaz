package ar.edu.unq.po2.tpStateStrategy.reproductorMP3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SongTest {

	private Song cancion;

	@BeforeEach
	void setUp() throws Exception {
		cancion = new Song("more");
		
	}

	@Test
	public void testnombre() {
		assertEquals("more", cancion.getNombre());
	}
	
	@Test
	public void testCancionNoEstaEnReproduccion() {
		assertFalse(cancion.estaEnReproduccion());
	}

	@Test
	public void testPonerPlay2VecesSoloSigueLaReproduccion() {
		cancion.play();
		cancion.play();
		assertTrue(cancion.estaEnReproduccion());
	}
	
	@Test
	public void testPauseAUnaCancionEnReproduccion() {
		cancion.play();
		cancion.pause();
		assertFalse(cancion.estaEnReproduccion());
	}
	
	@Test
	public void testStopAUnaCancionEnReproduccion() {
		cancion.play();
		cancion.stop();
		assertFalse(cancion.estaEnReproduccion());
	}
	
	@Test
	public void testPonerPlayAUnaCancionEnPausa() {
		cancion.play();
		cancion.pause();
		cancion.play();
		assertTrue(cancion.estaEnReproduccion());
	}
	
	@Test
	public void testPonerPausaAUnaCacnionPausadaLaReproduce() {
		cancion.play();
		cancion.pause();
		cancion.pause();
		assertTrue(cancion.estaEnReproduccion());
	}
	
	@Test
	public void testPonerStopACancionEnPausaLaFrena() {
		cancion.play();
		cancion.pause();
		cancion.stop();
		assertFalse(cancion.estaEnReproduccion());
	}
	
	@Test
	public void testPonerPauseOStopAUnaCancionEnStopNoHaceNada() {
		cancion.pause();
		cancion.stop();
		assertFalse(cancion.estaEnReproduccion());
	}
}
