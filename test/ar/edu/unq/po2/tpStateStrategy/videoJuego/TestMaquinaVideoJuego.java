package ar.edu.unq.po2.tpStateStrategy.videoJuego;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestMaquinaVideoJuego {
	
	private MaquinaVideoJuego maquina;



	@BeforeEach
	private void setUp() throws Exception {
		maquina = new MaquinaVideoJuego();
	}
	
	@Test
	public void testEnciendoLaMaquinaSinPonerFicha() {
		assertEquals("Ingrese ficha", maquina.encenderMaquina());
	}

	@Test
	public void testIngresoUnaMonedaYEnciendoLaMaquina() {
		maquina.ingresarMoneda();
		assertEquals("Comienza juego para un solo jugador", maquina.encenderMaquina());
	}

	@Test
	public void testIngresoDosmonedasYEnciendoLaMaquina() {
		maquina.ingresarMoneda();
		maquina.ingresarMoneda();
		assertEquals("Comienza juego para dos jugadores", maquina.encenderMaquina());
	}
	
	@Test
	public void testTerminoElJuego() {
		maquina.terminoElJuego();
		assertEquals(0, maquina.getCantidadMonedas());
		assertTrue(maquina.getEstado() instanceof BotonInicioSinMonedas);
	}
}
