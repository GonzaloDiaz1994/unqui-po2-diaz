package ar.edu.unq.po2.tpTemplate;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tpTemplate.sueldosRecargado.EmpleadoDePlanta;
import ar.edu.unq.po2.tpTemplate.sueldosRecargado.EmpleadoPasante;
import ar.edu.unq.po2.tpTemplate.sueldosRecargado.EmpleadoTemporario;
import ar.edu.unq.po2.tpTemplate.sueldosRecargado.Empresa;

class EmpleadosTest {

	private EmpleadoTemporario temporario;
	private EmpleadoPasante pasante;
	private EmpleadoDePlanta planta;
	
	private Empresa sony;
	
	@BeforeEach
	void setUp() throws Exception {
		
		temporario = new EmpleadoTemporario(10, 2, true);
		pasante = new EmpleadoPasante(20);
		planta = new EmpleadoDePlanta(3);
		
		sony = new Empresa();
	}
	
	@Test
	public void sueldoEmpleadoTemporario() {
		assertEquals(1000.5, temporario.sueldo());
	}
	
	@Test
	public void sueldoEmpleadoPasante() {
		assertEquals(696, pasante.sueldo());
	}

	@Test
	public void sueldoEmpleadoDePlanta() {
		assertEquals(3001.5, planta.sueldo());
	}
	
	@Test
	public void empresaPagaTodosLosSueldos() {
		sony.agregarEmpleado(temporario);
		sony.agregarEmpleado(pasante);
		sony.agregarEmpleado(planta);
		assertEquals(4698, sony.pagarSueldos());
	}
	
}
