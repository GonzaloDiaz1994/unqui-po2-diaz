package ar.edu.unq.po2.tpTemplate.wikipedia;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WikipediaTest {

	private PaginaDeWikipedia pagina1;
	private PaginaDeWikipedia pagina2;
	private PaginaDeWikipedia pagina3;
	private PaginaDeWikipedia pagina4;
	
	private MismaLetraInicial mismaLetra;
	private LinkEnComun linkEnComun;
	private PropiedadEnComun propiedadEnComun;
	
	private List<WikipediaPage> paginas;
	private List<WikipediaPage> resultado;
	
	@BeforeEach
	void setUp() throws Exception {
		pagina1 = new PaginaDeWikipedia("Botellas");
		pagina2 = new PaginaDeWikipedia("Plastico");
		pagina3 = new PaginaDeWikipedia("Bolsa");
		pagina4 = new PaginaDeWikipedia("Basura");
		
		pagina1.agregarLink(pagina2);
		pagina1.agregarLink(pagina3);
		pagina1.agregarLink(pagina4);
		
		pagina4.agregarLink(pagina3);
		
		pagina1.agregarInfobox("1", pagina2);
		pagina1.agregarInfobox("2", pagina3);
		
		pagina2.agregarInfobox("2", pagina3);
		pagina2.agregarInfobox("4", pagina1);
		
		pagina3.agregarInfobox("5", pagina1);
		pagina3.agregarInfobox("6", pagina2);
		
		paginas = new ArrayList<WikipediaPage>();
		paginas.add(pagina1);
		paginas.add(pagina2);
		paginas.add(pagina3);
		paginas.add(pagina4);
		
		resultado = new ArrayList<WikipediaPage>();
		
		mismaLetra = new MismaLetraInicial();
		linkEnComun = new LinkEnComun();
		propiedadEnComun = new PropiedadEnComun();
		
	}

	@Test
	public void mismaLetraTest() {
		resultado.add(pagina1);
		resultado.add(pagina3);
		resultado.add(pagina4);
		assertEquals(resultado, mismaLetra.getSimilarPages(pagina3,paginas));
	}
	
	@Test
	public void linkEnComuntest() {
		resultado.add(pagina1);
		resultado.add(pagina4);
		assertEquals(resultado, linkEnComun.getSimilarPages(pagina1, paginas));
	}

	@Test
	public void propiedadEnComunTest() {
		resultado.add(pagina1);
		resultado.add(pagina2);
		assertEquals(resultado, propiedadEnComun.getSimilarPages(pagina1, paginas));
		
		
	}
}
