package ar.edu.unq.po2.tpTestDoubles;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CartaTest {

	private Carta carta; //SUT
	private Carta cartaStub;
	
	
	
	@BeforeEach
	void setUp() throws Exception {
	
		carta = new Carta(7, "Trebol");
		cartaStub = mock(Carta.class);
		
		when(cartaStub.getValor()).thenReturn(9);
		when(cartaStub.getPalo()).thenReturn("Pica");
	}

	//EXERCISE
	@Test
	public void testValor() {
		assertEquals(7, carta.getValor());
	}
	
	@Test
	public void testPalo() {
		assertEquals("Trebol", carta.getPalo());
	}
	
	@Test
	public void testEsMayorADaFalse() {
		assertFalse(carta.esMayorA(cartaStub));
	}

	@Test
	public void testEsMayorADaTrue() {
		when(cartaStub.getValor()).thenReturn(5);
		assertTrue(carta.esMayorA(cartaStub));
	}
	
	@Test
	public void testEsMismoPaloDaFalse() {
		assertFalse(carta.esMismoPaloA(cartaStub));
	}
	
	@Test
	public void testEsMimsmoPaloDaTrue() {
		when(cartaStub.getPalo()).thenReturn("Trebol");
		assertTrue(carta.esMismoPaloA(cartaStub));
	}	
	
}
