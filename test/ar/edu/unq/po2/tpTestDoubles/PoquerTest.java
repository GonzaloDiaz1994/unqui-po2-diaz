package ar.edu.unq.po2.tpTestDoubles;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

class PoquerTest {

	//Setup
	private PokerStatus mano;
	
	private Carta cartaStub1;
	private Carta cartaStub2;
	private Carta cartaStub3;
	private Carta cartaStub4;
	private Carta cartaStub5;
	
	private List<Carta> cartas;

	@BeforeEach
	void setUp() throws Exception { // TEARDOWN
		
		cartas = new ArrayList<Carta>();
		
		cartaStub1 = mock(Carta.class);
		cartaStub2 = mock(Carta.class);
		cartaStub3 = mock(Carta.class);
		cartaStub4 = mock(Carta.class);
		cartaStub5 = mock(Carta.class);
		
		mano = new PokerStatus(cartas); // SUT
		
	//Exercise
		cartas.add(cartaStub1);
		cartas.add(cartaStub2);
		cartas.add(cartaStub3);
		cartas.add(cartaStub4);
		cartas.add(cartaStub5);		
	}
	
	//Verify
	@Test
	public void testVerificacionHayPoquer() {
		when(cartaStub1.getPalo()).thenReturn("Pica");
		when(cartaStub2.getPalo()).thenReturn("Pica");
		when(cartaStub3.getPalo()).thenReturn("Pica");
		when(cartaStub4.getPalo()).thenReturn("Pica");
		when(cartaStub5.getPalo()).thenReturn("Trebol");
		assertEquals("Poquer", mano.verificar());
	}
	
	
	@Test
	public void testVerificacionHayColor() {
		when(cartaStub1.getPalo()).thenReturn("Pica");
		when(cartaStub2.getPalo()).thenReturn("Pica");
		when(cartaStub3.getPalo()).thenReturn("Pica");
		when(cartaStub4.getPalo()).thenReturn("Pica");
		when(cartaStub5.getPalo()).thenReturn("Pica");
		assertEquals("Color", mano.verificar());
	}
	@Test
	public void testVerificacionHayTrio() {
		when(cartaStub1.getValor()).thenReturn(3);
		when(cartaStub2.getValor()).thenReturn(4);
		when(cartaStub3.getValor()).thenReturn(3);
		when(cartaStub4.getValor()).thenReturn(7);
		when(cartaStub5.getValor()).thenReturn(3);
		assertEquals("Trio", mano.verificar());
	}
	@Test
	public void testVerificacionEsNada() {
		when(cartaStub1.getPalo()).thenReturn("Pica");
		when(cartaStub2.getPalo()).thenReturn("Trebol");
		when(cartaStub3.getPalo()).thenReturn("Corazones");
		when(cartaStub4.getPalo()).thenReturn("Diamante");
		when(cartaStub5.getPalo()).thenReturn("Trebol");
		assertEquals("Nada", mano.verificar());
	}
	
}













